# Python Script Assistant Brainstorming Ideas

## Themes

* Create a programmatic way to update the themes, with the goal to run the
  themes through a validation process.

## Ansi

* Test what happens if you highlight(highlight(text)), this may not work as expected.
  * If not, the fix will be re-ordering the ANSI codes, which will break a lot of unit tests.
* Make an ansi TypedDict to use in other themes

## Log

* Theme needs to be a deep copy of the default.

## Menu

* Multi-Column output
  * Was thinking true/false for multi-column flag, and just use whole console width
* Have way to insert blank lines when displaying menu lines
  * Put "" in menu items? or None?
* To set default menu item, start description with a "*"
  * Have flag for changing default to last chosen item
* Case-insensitive menu selection:
  * If key not found, loop thru it and check insensitive (just automatically do this)
* Theme is at class level, but be able to override it at an object level.
* What if user only wants keys (ie, doesn't want numbers, or to provide text with keys)
  * How to indicate this
* repr methods need to handle quotes in values

## Text Utilities

* Text justification: left, center, right
  * As long as we can get the terminal size without ANSI commands (yes we can)
* Text drawing: underline, overline, box, wings
  * Can be ASCII or UTF-8 characters
  * If we do this, want to do it without ANSI cursor commands

## Linux Convenience

* Download a file (wget, curl, urllib, requests)
* Does it save the developer any work to make convenience methods for common
  linux commands?
  * ls, ps, figlet, banner, cron
* Docker assistant
* Qemu / VirtualBox assistant

## Development

* When Python 3.11 becomes the minimum maintained version, StrEnum is
  available to use for constants

## General Requirements

* How to use this library should be obvious
* Modules / Classes should be usable immediately after import with minimal
  configuration
* The developers don't need to spend time with setup, but at the 
  same time, give some options for customizations
* Theming should control formatting as well as colors (like properties)
* Default themes assume a black background with white text
* Be careful adding too many features, this is to help with scripting,
  not necessarily to write full applications
* This library should not require any third-party libraries, so that it can
  run on the standard Python library alone.

## Windows

* Windows is a low priority to this project.
* Other than the shell module, do some testing to validate the other modules
  work on Windows.
* Add Powershell module?
* Add command prompt module?

## Rejected (or way down the road)

* Validations module
  * There are many validation modules out there, and it takes a significant
    amount of time to write and test these.
  * Why would we want this?
    * Partially to validate all parameters within script assistant itself.
    * But maybe it's better to look at external options, like pydantic.
    * I feel this particular module will lead down a large rabbit hole
      of work and scope creep.
