from typing import Optional, Any
from script_assistant.validators.none import validate_none


def validate_bool(value: Optional[Any],
                  field_name: str,
                  none_allowed: bool = False,
                  ) -> bool:

    if value is None:
        return validate_none(value, none_allowed=none_allowed, field_name=field_name)

    # check type
    if not isinstance(value, bool):
        if field_name is None:
            return False
        else:
            error_msg = f"{field_name} is not a boolean: {value}"
            raise TypeError(error_msg)

    return True
