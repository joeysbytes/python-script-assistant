from typing import Optional

from script_assistant.Constant import Constant


# TODO: add regex checking
def validate_str(text: str,
                 none_allowed: bool = False,
                 empty_allowed: bool = False,
                 check_type: bool = True,
                 field_name: Optional[str] = None) -> bool:
    valid = True

    # check if string
    if valid and check_type and (not isinstance(text, str)):
        if field_name is None:
            valid = False
        else:
            error_msg = f"{field_name} is not a string: {type(text)}"
            raise TypeError(error_msg)

    # check if text is None
    if valid and (not none_allowed) and (text is None):
        if field_name is None:
            valid = False
        else:
            error_msg = f"{field_name} is None"
            raise ValueError(error_msg)

    # check if text is empty
    if valid and (not empty_allowed) and (text == Constant.EMPTY_STRING.value):
        if field_name is None:
            valid = False
        else:
            error_msg = f"{field_name} is empty"
            raise ValueError(error_msg)
    return valid
