from typing import Optional, Union
from script_assistant.validators.none import validate_none


def validate_range(value: Union[int, float],
                   min_value: Optional[Union[int, float]] = None,
                   max_value: Optional[Union[int, float]] = None,
                   field_name: Optional[str] = None) -> bool:
    if min_value is not None:
        valid = valid_range(value,
                            min_value=min_value)
        if not valid:
            error_msg = f"{field_name} < minimum value: {value} < {min_value}"
            raise ValueError(error_msg)
    if max_value is not None:
        valid = valid_range(value,
                            max_value=max_value)
        if not valid:
            error_msg = f"{field_name} > maximum value: {value} > {max_value}"
            raise ValueError(error_msg)
    return True


def valid_range(value: Union[int, float],
                min_value: Optional[Union[int, float]] = None,
                max_value: Optional[Union[int, float]] = None) -> bool:
    if min_value is not None:
        if value < min_value:
            return False
    if max_value is not None:
        if value > max_value:
            return False
    return True
