from typing import Optional, Any
from script_assistant.validators.none import validate_none


def validate_float(value: Optional[Any],
                   min_value: Optional[float] = None,
                   max_value: Optional[float] = None,
                   int_allowed: bool = False,
                   str_allowed: bool = False,
                   bool_allowed: bool = False,
                   none_allowed: bool = False,
                   field_name: Optional[str] = None) -> bool:

    if value is None:
        return validate_none(value, none_allowed=none_allowed, field_name=field_name)

    vl: float

    # check type
    if bool_allowed and isinstance(value, bool):
        vl = float(value)
    elif int_allowed and isinstance(value, int):
        vl = float(value)
    elif str_allowed and isinstance(value, str):
        try:
            vl = float(value)
        except ValueError:
            if field_name is None:
                return False
            else:
                error_msg = f"{field_name} is not a valid string float: {value}"
                raise ValueError(error_msg)
    else:
        if isinstance(value, float):
            vl = value
        else:
            if field_name is None:
                return False
            else:
                error_msg = f"{field_name} is not a float: {value}"
                raise TypeError(error_msg)

    # check range
    if min_value is not None:
        if vl < min_value:
            if field_name is None:
                return False
            else:
                error_msg = f"{field_name} < minimum value: {value} < {min_value}"
                raise ValueError(error_msg)
    if max_value is not None:
        if vl > max_value:
            if field_name is None:
                return False
            else:
                error_msg = f"{field_name} > maximum value: {value} > {max_value}"
                raise ValueError(error_msg)

    return True
