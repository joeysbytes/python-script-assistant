from typing import Optional, Any


def validate_none(value: Optional[Any],
                  none_allowed: bool = False,
                  field_name: Optional[str] = None) -> bool:

    if (not none_allowed) and (value is None):
        if field_name is None:
            return False
        else:
            error_msg = f"{field_name} is None"
            raise ValueError(error_msg)
    return True
