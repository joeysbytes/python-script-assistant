from typing import Optional, Any
from script_assistant.validators.none import validate_none
from script_assistant.validators.number import validate_range


def validate_int(value: Optional[Any],
                 min_value: Optional[int] = None,
                 max_value: Optional[int] = None,
                 float_allowed: bool = False,
                 str_allowed: bool = False,
                 bool_allowed: bool = False,
                 none_allowed: bool = False,
                 field_name: Optional[str] = None) -> bool:

    # check none
    if value is None:
        return validate_none(value, none_allowed=none_allowed, field_name=field_name)

    vl: int

    # check type
    if bool_allowed and isinstance(value, bool):
        vl = int(value)
    elif float_allowed and isinstance(value, float):
        vl = int(value)
    elif str_allowed and isinstance(value, str):
        try:
            if float_allowed and ("." in value):
                vl = int(float(value))
            else:
                vl = int(value)
        except ValueError:
            if field_name is None:
                return False
            else:
                error_msg = f"{field_name} is not a valid string integer: {value}"
                raise ValueError(error_msg)
    else:
        if (isinstance(value, int)) and (not isinstance(value, bool)):
            vl = value
        else:
            if field_name is None:
                return False
            else:
                error_msg = f"{field_name} is not an integer: {value}"
                raise TypeError(error_msg)

    # check range
    return validate_range(vl,
                          min_value=min_value,
                          max_value=max_value,
                          field_name=field_name)
