from typing_extensions import TypedDict
from typing import Dict, List
from script_assistant.ansi import Color


class TimestampType(TypedDict):
    format: str
    highlight: Dict
    prefix: str
    prefix_highlight: Dict
    suffix: str
    suffix_highlight: Dict


class IndividualLevelType(TypedDict):
    text: str
    highlight: Dict


class LevelType(TypedDict):
    levels: List[IndividualLevelType]
    prefix: str
    prefix_highlight: Dict
    suffix: str
    suffix_highlight: Dict


class TextType(TypedDict):
    highlight: Dict
    prefix: str
    prefix_highlight: Dict
    suffix: str
    suffix_highlight: Dict


class ThemeType(TypedDict):
    format: str
    timestamp: TimestampType
    level: LevelType
    text: TextType


THEME: ThemeType = {
    "format": "{timestamp} {level}: {text}",

    "timestamp": {
        "format": "%Y%m%d %H%M%S",
        "highlight": {"fg": Color.GREEN},
        "prefix": "[",
        "prefix_highlight": {"fg": Color.CYAN},
        "suffix": "]",
        "suffix_highlight": {"fg": Color.CYAN}
    },

    "level": {
        "levels": [
            {"text": "trace", "highlight": {"fg": Color.MAGENTA}},
            {"text": "debug", "highlight": {"fg": Color.CYAN}},
            {"text": "info ", "highlight": {}},
            {"text": "Warn ", "highlight": {"fg": Color.BRIGHT_YELLOW}},
            {"text": "ERROR", "highlight": {"fg": Color.BRIGHT_YELLOW, "bg": Color.RED}},
            {"text": "CRIT ", "highlight":
                {"fg": Color.BRIGHT_WHITE, "bg": Color.BRIGHT_MAGENTA, "intensity": 2}},
            {"text": "FATAL", "highlight":
                {"fg": Color.BRIGHT_RED, "bg": Color.BRIGHT_YELLOW, "intensity": 2, "blink": True}}
        ],
        "prefix": "",
        "prefix_highlight": {},
        "suffix": "",
        "suffix_highlight": {},
    },

    "text": {
        "highlight": {},
        "prefix": "",
        "prefix_highlight": {},
        "suffix": "",
        "suffix_highlight": {}
    }
}
