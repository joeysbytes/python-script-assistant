from typing import Union, List, Dict

from script_assistant.menu.MenuItem import MenuItem


class MenuGroup:

    # Object Initialization ###############################################

    def __init__(self, items: Union[List[str], Dict[str, str]]):
        self._max_key_length = 0
        self._max_text_length = 0
        self._num_items = 0
        self._menu_items = {}
        MenuGroup._validate_items(items)
        if isinstance(items, List):
            self._load_list_items(items)
        else:  # isinstance(items, Dict)
            self._load_dict_items(items)

    @staticmethod
    def _validate_items(items: Union[List[str], Dict[str, str]]) -> None:
        if (items is None) or ((not isinstance(items, List)) and (not isinstance(items, Dict))):
            error_msg = f"Menu items must be List or Dict: {type(items)}"
            raise TypeError(error_msg)
        elif len(items) == 0:
            error_msg = f"Menu items list is empty: {items}"
            raise ValueError(error_msg)

    def _load_list_items(self, items: List[str]) -> None:
        for index, text in enumerate(items):
            self._add_menu_item(key=str(index+1), text=text, index=index)

    def _load_dict_items(self, items: Dict[str, str]) -> None:
        for index, (key, text) in enumerate(items.items()):
            self._add_menu_item(key, text, index)

    def _add_menu_item(self, key: str, text: str, index: int) -> None:
        menu_item = MenuItem(key=key, text=text, index=index)
        if menu_item.key in self._menu_items.keys():
            error_msg = f"Menu key already exists: {menu_item.key}"
            raise ValueError(error_msg)
        self._menu_items[menu_item.key] = menu_item
        self._max_key_length = max(menu_item.key_length, self.max_key_length)
        self._max_text_length = max(menu_item.text_length, self.max_text_length)
        self._num_items += 1

    # Dunder Methods ######################################################

    def __str__(self) -> str:
        text = ""
        for index, (key, menu_item) in enumerate(self._menu_items.items()):
            text += str(menu_item)
            if index < self.num_items - 1:
                text += '\n'
        return text

    def __repr__(self) -> str:
        items_dict = {}
        for key, menu_item in self._menu_items.items():
            items_dict[menu_item.key] = menu_item.text
        text = "MenuGroup(items="
        text += str(items_dict)
        text += ")"
        return text

    # Properties ##########################################################

    @property
    def max_key_length(self) -> int:
        return self._max_key_length

    @property
    def max_text_length(self) -> int:
        return self._max_text_length

    @property
    def num_items(self) -> int:
        return self._num_items
