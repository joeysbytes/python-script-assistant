from script_assistant.ansi import Color

THEME = {
    "title": {
        "format": "{title}\n\n",
        "highlight": {"fg": Color.BRIGHT_YELLOW, "underline": True}
    },

    "pre_text": {
        "format": "{pre_text}\n\n",
        "highlight": {"fg": Color.CYAN}
    },

    "post_text": {
        "format": "{post_text}\n\n",
        "highlight": {"fg": Color.CYAN}
    },

    "prompt": {
        "format": "{prompt} ",
        "highlight": {"fg": Color.WHITE, "italic": True},
        "separator": ":",
        "separator_highlight": {"fg": Color.YELLOW}
    },

    "items": {
        "format": "{items}\n\n",
        "item": {
            "format": "{default}{number}{separator} {item}\n",
            "highlight": {"fg": Color.WHITE},
            "default_char": "*",
            "default_char_highlight": {"fg": Color.MAGENTA},
            "key_highlight": {"fg": Color.BRIGHT_GREEN},
            "separator": ")",
            "separator_highlight": {"fg": Color.BRIGHT_BLUE}
        }
    }
}
