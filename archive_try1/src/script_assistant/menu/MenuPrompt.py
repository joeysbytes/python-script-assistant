from script_assistant.Constant import Constant

# TODO: after 3 failed attempts, redisplay the menu


class MenuPrompt:

    def __init__(self, prompt: str):
        self.prompt = prompt

    # TODO: __str__, __repr__

    @property
    def prompt(self) -> str:
        return self._prompt

    @prompt.setter
    def prompt(self, text: str) -> None:
        if text is None:
            self._prompt = Constant.EMPTY_STRING.value
        else:
            self._prompt = str(text)
