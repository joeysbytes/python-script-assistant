from typing import Optional


class MenuText:

    # Initialize ##########################################################

    def __init__(self, text: Optional[str], field: str = "Unknown",
                 empty_allowed: bool = False, none_allowed: bool = True):
        self._empty_allowed = MenuText._validate_bool(empty_allowed, "empty_allowed")
        self._none_allowed = MenuText._validate_bool(none_allowed, "none_allowed")
        self._field = str(field)
        self._text = MenuText._validate_text(text)

    @staticmethod
    def _validate_bool(value: bool, parameter: str) -> bool:
        if isinstance(value, bool) and (not isinstance(value, int)):
            return value
        else:
            error_msg = f"Invalid boolean for {parameter}: {value}"
            raise TypeError(error_msg)

    def _validate_text(self, text: str) -> str:
        if (text is None) and (not self.none_allowed):
            pass

    # Properties ##########################################################

    @property
    def empty_allowed(self) -> bool:
        return self._empty_allowed

    @property
    def none_allowed(self) -> bool:
        return self._none_allowed

    @property
    def field(self) -> str:
        return self._field
