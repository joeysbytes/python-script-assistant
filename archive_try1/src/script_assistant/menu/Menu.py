# Timer to submit default option if user doesn't press a key?
#   Make this an option when displaying menu, not a property

from typing import List, Optional, Tuple, Dict, Union
from script_assistant.Constant import Constant
from script_assistant.menu.MenuGroup import MenuGroup

from script_assistant.menu.menu_theme import THEME as _THEME
from script_assistant.ansi import highlight
from script_assistant.menu.MenuGroup import MenuGroup
from script_assistant.menu.MenuPrompt import MenuPrompt
import copy


class Menu:

    THEME = copy.deepcopy(_THEME)

    # Standard Class Methods ##############################################

    # TODO: items can be a Tuple or Set (or check for others)
    def __init__(self,
                 items: Union[List[str], Dict[str, str]],
                 title: Optional[str] = None,
                 pre_text: Optional[str] = None,
                 post_text: Optional[str] = None,
                 prompt: str = "Enter Choice"):
        self._menu_group = MenuGroup(items=items)
        self._menu_prompt = MenuPrompt(prompt=prompt)

        # self.title = title
        # self.pre_text = pre_text
        # self.post_text = post_text
        # self.prompt = prompt
        # self._theme = None

    # TODO: Write __repr__, __str__ and unit tests
