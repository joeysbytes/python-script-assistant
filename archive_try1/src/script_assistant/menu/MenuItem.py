class MenuItem:

    # Object Initialization #######################################

    def __init__(self, key: str, text: str, index: int):
        self._key = MenuItem._init_key(key)
        self._key_length = len(self._key)
        self._text = str(text)
        self._text_length = len(self._text)
        self._index = MenuItem._init_index(index)

    @staticmethod
    def _init_key(key: str) -> str:
        key_str = str(key)
        if len(key_str.strip()) == 0:
            error_msg = f"Menu Item key is empty or blank: '{key_str}'"
            raise ValueError(error_msg)
        return key_str

    @staticmethod
    def _init_index(index: int) -> int:
        if (not isinstance(index, int)) or (isinstance(index, bool)):
            error_msg = f"Menu Item index is not an int: {type(index)}"
            raise TypeError(error_msg)
        elif index < 0:
            error_msg = f"Menu Item index < 0: {index}"
            raise ValueError(error_msg)
        return index

    # Dunder Methods ######################################################

    def __str__(self) -> str:
        text = f"({self.index}) {self.key}: {self.text}"
        return text

    def __repr__(self) -> str:
        text = f'MenuItem(key="{self.key}", text="{self.text}", index={self.index})'
        return text

    # Properties ##########################################################

    @property
    def key(self) -> str:
        return self._key

    @property
    def key_length(self) -> int:
        return self._key_length

    @property
    def text(self) -> str:
        return self._text

    @property
    def text_length(self) -> int:
        return self._text_length

    @property
    def index(self) -> int:
        return self._index
