from enum import IntEnum
from script_assistant.themes.log import THEME
from script_assistant.ansi import highlight
from datetime import datetime
import sys
from typing_extensions import TypedDict


# Enums ###################################################################

class LogLevel(IntEnum):
    TRACE = 0
    DEBUG = 1
    INFO = 2
    WARNING = 3
    WARN = 3
    ERROR = 4
    CRITICAL = 5
    CRIT = 5
    FATAL = 6


# Configuration ###########################################################

_ConfigType = TypedDict("_ConfigType", {"enabled": bool, "log_level": int})
_CONFIG: _ConfigType = {
    "enabled": True,
    "log_level": LogLevel.WARNING.value
}


# Constants ###############################################################

MIN_LOG_LEVEL = 0
MAX_LOG_LEVEL = 6


# Enabled #################################################################

def enable() -> None:
    _CONFIG["enabled"] = True


def disable() -> None:
    _CONFIG["enabled"] = False


def is_enabled() -> bool:
    return _CONFIG["enabled"]


def is_disabled() -> bool:
    return not _CONFIG["enabled"]


# Log Level ###############################################################

def _check_level(level: int) -> None:
    if (not isinstance(level, int)) or (isinstance(level, bool)):
        error_msg = f"Log level is not an integer: {level}"
        raise TypeError(error_msg)
    if not (MIN_LOG_LEVEL <= level <= MAX_LOG_LEVEL):
        error_msg = f"Log level must be between {MIN_LOG_LEVEL} and {MAX_LOG_LEVEL}: {level}"
        raise ValueError(error_msg)


def set_level(level: int) -> None:
    _check_level(level)
    _CONFIG["log_level"] = level


def get_level() -> int:
    return _CONFIG["log_level"]


# Logging #################################################################

def _log(log_level: int, txt: str) -> None:
    if (is_disabled()) or (log_level < get_level()):
        return

    timestamp = _build_timestamp()
    level = _build_level(log_level)
    text = _build_text(txt)
    log_msg = THEME["format"].format(timestamp=timestamp,
                                     level=level,
                                     text=text)

    if log_level >= LogLevel.WARNING:
        print(log_msg, file=sys.stderr)
    else:
        print(log_msg, file=sys.stdout)


def log(level: int, text: str) -> None:
    if is_enabled():
        _check_level(level)
        _log(level, text)


def trace(text: str) -> None:
    _log(LogLevel.TRACE, text)


def debug(text: str) -> None:
    _log(LogLevel.DEBUG, text)


def info(text: str) -> None:
    _log(LogLevel.INFO, text)


def warning(text: str) -> None:
    _log(LogLevel.WARNING, text)


def warn(text: str) -> None:
    _log(LogLevel.WARN, text)


def error(text: str) -> None:
    _log(LogLevel.ERROR, text)


def critical(text: str) -> None:
    _log(LogLevel.CRITICAL, text)


def crit(text: str) -> None:
    _log(LogLevel.CRIT, text)


def fatal(text: str) -> None:
    _log(LogLevel.FATAL, text)


# Builders ################################################################

def _build_timestamp() -> str:
    theme = THEME["timestamp"]
    ts = _get_current_timestamp().strftime(theme["format"])
    ts_text = highlight(theme["prefix"], **theme["prefix_highlight"])
    ts_text += highlight(ts, **theme["highlight"])
    ts_text += highlight(theme["suffix"], **theme["suffix_highlight"])
    return ts_text


# This function is broken out so that it can be overridden in unit tests.
def _get_current_timestamp() -> datetime:
    return datetime.now()


def _build_level(level: int) -> str:
    theme = THEME["level"]
    txt = theme["levels"][level]["text"]
    hl = theme["levels"][level]["highlight"]
    lvl_text = highlight(theme["prefix"], **theme["prefix_highlight"])
    lvl_text += highlight(txt, **hl)
    lvl_text += highlight(theme["suffix"], **theme["suffix_highlight"])
    return lvl_text


def _build_text(txt: str) -> str:
    theme = THEME["text"]
    text = highlight(theme["prefix"], **theme["prefix_highlight"])
    text += highlight(txt, **theme["highlight"])
    text += highlight(theme["suffix"], **theme["suffix_highlight"])
    return text
