from enum import Enum


class Constant(Enum):

    EMPTY_STRING = ""
    SPACE = " "
    NEWLINE = '\n'
