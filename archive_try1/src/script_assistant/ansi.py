from enum import IntEnum
from typing import Optional, Tuple, List
from script_assistant.Constant import Constant
from typing_extensions import TypedDict


# Enums ###################################################################

class Color(IntEnum):
    DEFAULT = -1
    BLACK = 0
    RED = 1
    GREEN = 2
    YELLOW = 3
    BLUE = 4
    MAGENTA = 5
    CYAN = 6
    WHITE = 7
    BRIGHT_BLACK = 8
    GRAY = 8
    GREY = 8
    BRIGHT_RED = 9
    BRIGHT_GREEN = 10
    BRIGHT_YELLOW = 11
    BRIGHT_BLUE = 12
    BRIGHT_MAGENTA = 13
    BRIGHT_CYAN = 14
    BRIGHT_WHITE = 15

    def color_value(self) -> int:
        return self.value

    def ansi_fg_value(self) -> int:
        # 30 - 37, 90 - 97, 39
        if self.value == -1:
            return 39
        else:
            return (self.value + 30) + (52 * (1 if self.value >= 8 else 0))

    def ansi_bg_value(self) -> int:
        # 40 - 47, 100 - 107, 49
        return self.ansi_fg_value() + 10


class Intensity(IntEnum):
    DIM = 0
    NORMAL = 1
    BOLD = 2
    BRIGHT = 2


class _Attribute(IntEnum):
    INTENSITY_DIM = 2
    INTENSITY_NORMAL = 22
    INTENSITY_BOLD = 1
    ITALIC = 3
    UNDERLINE = 4
    CROSSOUT = 9
    REVERSE = 7
    CONCEAL = 8
    BLINK = 5

    def on_value(self) -> int:
        return self.value

    def off_value(self) -> int:
        if (self.value == 1) or (self.value == 2) or (self.value == 22):
            return 22
        else:
            return self.on_value() + 20


# Configuration ###########################################################

_ConfigType = TypedDict("_ConfigType", {"enabled": bool, "default_fg": int, "default_bg": int})
_CONFIG: _ConfigType = {
    "enabled": False,
    "default_fg": Color.DEFAULT.color_value(),
    "default_bg": Color.DEFAULT.color_value()
}

# Constants ###############################################################

_CSI = '\033' + '['
_EMPTY_STRING = Constant.EMPTY_STRING.value
MIN_COLOR_INDEX = -1
MAX_COLOR_INDEX = 15
MIN_INTENSITY = 0
MAX_INTENSITY = 2


# Enable / Disable ANSI ###################################################

def enable() -> None:
    _CONFIG["enabled"] = True


def disable() -> None:
    _CONFIG["enabled"] = False


def is_enabled() -> bool:
    return _CONFIG["enabled"]


def is_disabled() -> bool:
    return not _CONFIG["enabled"]


# Color Defaults ##########################################################

def _check_color_index(color_index: int, background: bool = False) -> None:
    if (not isinstance(color_index, int)) or (isinstance(color_index, bool)):
        field = "Background" if background else "Foreground"
        error_msg = f"{field} color index is not an integer: '{color_index}'"
        raise TypeError(error_msg)
    if not MIN_COLOR_INDEX <= color_index <= MAX_COLOR_INDEX:
        field = "Background" if background else "Foreground"
        error_msg = (f"{field} color index must be between {MIN_COLOR_INDEX} and {MAX_COLOR_INDEX}: " +
                     f"'{color_index}'")
        raise ValueError(error_msg)


def set_default_color(fg: Optional[int] = None, bg: Optional[int] = None) -> None:
    # default foreground
    if fg is not None:
        _check_color_index(fg, background=False)
        _CONFIG["default_fg"] = fg

    # default background
    if bg is not None:
        _check_color_index(bg, background=True)
        _CONFIG["default_bg"] = bg


def get_default_color() -> Tuple[int, int]:
    return get_default_fg(), get_default_bg()


def get_default_fg() -> int:
    return _CONFIG["default_fg"]


def get_default_bg() -> int:
    return _CONFIG["default_bg"]


# Highlight ###############################################################

def highlight(text: str, fg: Optional[int] = None, bg: Optional[int] = None,
              intensity: Optional[int] = None, italic: bool = False, underline: bool = False,
              crossout: bool = False, reverse: bool = False, conceal: bool = False,
              blink: bool = False, on_codes: bool = True, off_codes: bool = True) -> str:
    if is_disabled():
        return str(text)

    on_list: List[str] = []
    off_list: List[str] = []

    # foreground color
    if fg is not None:
        _check_color_index(fg, background=False)
        on_list.append(str(Color(fg).ansi_fg_value()))
        off_list.insert(0, str(Color(get_default_fg()).ansi_fg_value()))

    # background color
    if bg is not None:
        _check_color_index(bg, background=True)
        on_list.append(str(Color(bg).ansi_bg_value()))
        off_list.insert(0, str(Color(get_default_bg()).ansi_bg_value()))

    # intensity
    if intensity is not None:
        if (not isinstance(intensity, int)) or (isinstance(intensity, bool)):
            error_msg = f"Intensity is not an integer: '{intensity}'"
            raise TypeError(error_msg)
        if not MIN_INTENSITY <= intensity <= MAX_INTENSITY:
            error_msg = f"Intensity must be between {MIN_INTENSITY} and {MAX_INTENSITY}: '{intensity}'"
            raise ValueError(error_msg)
        # Some terminals need a normal code before a dim or bold code, because they treat
        # intensity as a relative scale instead of an absolute one. So we just always add a
        # normal code first.
        on_list.append(str(_Attribute.INTENSITY_NORMAL.on_value()))
        if intensity == 0:    # dim / faint
            on_list.append(str(_Attribute.INTENSITY_DIM.on_value()))
        elif intensity == 2:  # bold / bright
            on_list.append(str(_Attribute.INTENSITY_BOLD.on_value()))
        off_list.insert(0, str(_Attribute.INTENSITY_NORMAL.off_value()))

    # italic
    if italic:
        on_list.append(str(_Attribute.ITALIC.on_value()))
        off_list.insert(0, str(_Attribute.ITALIC.off_value()))

    # underline
    if underline:
        on_list.append(str(_Attribute.UNDERLINE.on_value()))
        off_list.insert(0, str(_Attribute.UNDERLINE.off_value()))

    # strikethrough / cross-out
    if crossout:
        on_list.append(str(_Attribute.CROSSOUT.on_value()))
        off_list.insert(0, str(_Attribute.CROSSOUT.off_value()))

    # reverse
    if reverse:
        on_list.append(str(_Attribute.REVERSE.on_value()))
        off_list.insert(0, str(_Attribute.REVERSE.off_value()))

    # conceal
    if conceal:
        on_list.append(str(_Attribute.CONCEAL.on_value()))
        off_list.insert(0, str(_Attribute.CONCEAL.off_value()))

    # blink
    if blink:
        on_list.append(str(_Attribute.BLINK.on_value()))
        off_list.insert(0, str(_Attribute.BLINK.off_value()))

    # return the ansi codes + text
    final_ansi = _EMPTY_STRING
    if on_codes and (len(on_list) > 0):
        on_str = ";".join(on_list)
        final_ansi += f"{_CSI}{on_str}m"
    final_ansi += str(text)
    if off_codes and (len(off_list) > 0):
        off_str = ";".join(off_list)
        final_ansi += f"{_CSI}{off_str}m"
    return final_ansi


# Clear Screen ############################################################

def clear_screen(fg: Optional[int] = None, bg: Optional[int] = None) -> None:
    # This function will validate fg and bg values for us
    set_default_color(fg=fg, bg=bg)
    if is_enabled():
        color_codes = highlight(_EMPTY_STRING, fg=get_default_fg(), bg=get_default_bg(), off_codes=False)
        clear_screen_code = f"{_CSI}2J"
        cursor_home_code = f"{_CSI}1;1H"
        print(f"{color_codes}{clear_screen_code}{cursor_home_code}", end=_EMPTY_STRING)


# Reset ###################################################################

def reset(clear: bool = False, full: bool = False) -> None:
    # This function will validate fg and bg values for us
    set_default_color(fg=Color.DEFAULT, bg=Color.DEFAULT)
    if is_enabled():
        print(f"{_CSI}0m", end=_EMPTY_STRING)
        if clear:
            clear_screen()
    if full:
        disable()
