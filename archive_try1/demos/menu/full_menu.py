from script_assistant import ansi, Menu
from script_assistant.ansi import Color as c

THEME = {
    "screen": {"fg": c.WHITE, "bg": c.BLACK},
    "main_title": {"fg": c.BRIGHT_YELLOW, "underline": True},
    "sub_title": {"fg": c.BLACK, "bg": c.CYAN}
}


def main():
    initialize()
    main_heading()

    # print(ansi.highlight("ANSI Codes Disabled", **THEME["sub_title"]))
    # print()
    # ansi.disable()
    # show_menu()
    # print()

    ansi.enable()
    print(ansi.highlight("ANSI Codes Enabled", **THEME["sub_title"]))
    print()
    show_menu()
    print()
    finalize()


def initialize():
    ansi.enable()
    ansi.clear_screen(**THEME["screen"])


def main_heading():
    title = "Full Menu"
    print(ansi.highlight(title, **THEME["main_title"]))
    print()


def finalize():
    ansi.reset(full=True)
    print()


def show_menu():
    items = ["Pandas",
             # "Koalas",
             # "Penguins",
             # "Dolphins",
             # "Giraffes",
             # "Kittens",
             # "Puppies",
             # "Turtles",
             # "Bunnies",
             # "Lemmings",
             # "Hedgehogs",
             # "Manatees",
             # "Pelicans",
             # "Kangaroos",
             # "Goldfish",
             "Newborn Calves",
             "Horses"]
    items.sort()

    full_menu = Menu(items,
                     title="Main Menu",
                     pre_text="Please select an animal below you would like to go hunt.",
                     post_text="You only get 1 license to kill.",
                     prompt="Select Animal")
    min_menu = Menu(items, prompt=None)

    print(full_menu)
    print(min_menu)


if __name__ == "__main__":
    main()
