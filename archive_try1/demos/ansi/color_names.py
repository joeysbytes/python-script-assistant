from script_assistant import ansi
from script_assistant.ansi import Color as c


THEME = {
    "screen": {"fg": c.WHITE, "bg": c.BLACK},
    "main_title": {"fg": c.BRIGHT_YELLOW, "underline": True},
    "column_heading": {"fg": c.BRIGHT_GREEN},
    "column_separator": {"fg": c.BRIGHT_YELLOW}
}


def main():
    initialize()
    main_heading()
    column_headings()
    table_rows()
    finalize()


def initialize():
    ansi.enable()
    ansi.clear_screen(**THEME["screen"])


def finalize():
    ansi.reset()
    ansi.disable()
    print()


def main_heading():
    title = "Color Names Chart"
    print(ansi.highlight(title, **THEME["main_title"]))
    print()


def column_headings():
    columns_txt = "Idx  Name            Foreground  Background"
    columns_und = "---  --------------  ----------  ----------"
    print(ansi.highlight(columns_txt, **THEME["column_heading"]))
    print(ansi.highlight(columns_und, **THEME["column_separator"]))


def table_rows():
    for name, color in c.__members__.items():
        color_idx = "{:>3}".format(color.value)
        color_name = "{:<14}".format(name)
        accent = c.BRIGHT_BLACK if color.value == c.BLACK else c.BLACK
        color_fg = ansi.highlight("Foreground", fg=color.value, bg=accent)
        color_bg = ansi.highlight("Background", fg=accent, bg=color.value)
        print(f"{color_idx}  {color_name}  {color_fg}  {color_bg}")


if __name__ == "__main__":
    main()
