from script_assistant import ansi
from script_assistant.ansi import Color as c


THEME = {
    "screen": {"fg": c.WHITE, "bg": c.BLACK},
    "main_title": {"fg": c.BRIGHT_YELLOW, "underline": True},
    "axis_title": {"fg": c.BRIGHT_GREEN},
    "axis_index": {"fg": c.WHITE}
}


def main():
    initialize()
    main_heading()
    chart_column_heading()
    chart_rows()
    finalize()


def initialize():
    ansi.enable()
    ansi.clear_screen(**THEME["screen"])


def finalize():
    ansi.reset()
    ansi.disable()
    print()


def main_heading():
    title = "Color Index Chart"
    print(ansi.highlight(title, **THEME["main_title"]))
    print()


def chart_column_heading():
    axis_title = "Foreground Index"
    print(" " * 5, end="")
    print(ansi.highlight(axis_title, **THEME["axis_title"]))
    print(" " * 5, end="")
    print(ansi.highlight("", **THEME["axis_index"], off_codes=False), end="")
    for idx in range(-1, 16):
        print("{:>3}".format(idx), end="")
    print()


def chart_rows():
    axis_title = "Background Index "
    cell_value = " # "
    for bg_idx in range(-1, 16):
        axis_char = ansi.highlight(axis_title[bg_idx+1:bg_idx+2], **THEME["axis_title"])
        idx_num = ansi.highlight("{:>2}".format(bg_idx), **THEME["axis_index"])
        print(f"{axis_char} {idx_num} ", end="")
        for fg_idx in range(-1, 16):
            cell = ansi.highlight(cell_value, fg=fg_idx, bg=bg_idx)
            print(cell, end="")
        print()


if __name__ == "__main__":
    main()
