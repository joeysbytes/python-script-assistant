from script_assistant import ansi
from script_assistant.ansi import Color as c


THEME = {
    "screen": {"fg": c.WHITE, "bg": c.BLUE},
    "main_title": {"fg": c.BRIGHT_WHITE, "underline": True},
    "column_heading": {"fg": c.BRIGHT_GREEN},
    "column_separator": {"fg": c.BRIGHT_YELLOW}
}


def main():
    initialize()
    main_heading()
    column_headings()
    table_rows()
    finalize()


def initialize():
    ansi.enable()
    ansi.clear_screen(**THEME["screen"])


def finalize():
    ansi.reset()
    ansi.disable()
    print()


def main_heading():
    title = "Attributes Chart"
    print(ansi.highlight(title, **THEME["main_title"]))
    print()


def column_headings():
    columns_txt = "Property                  Example Text"
    columns_und = "------------------------  ------------"
    print(ansi.highlight(columns_txt, **THEME["column_heading"]))
    print(ansi.highlight(columns_und, **THEME["column_separator"]))


def table_rows():
    ex = "Example Text"
    _print_table_row("intensity = 0 (dim)", ansi.highlight(ex, intensity=0))
    _print_table_row("intensity = 1 (normal)", ansi.highlight(ex, intensity=1))
    _print_table_row("intensity = 2 (bold)", ansi.highlight(ex, intensity=2))
    _print_table_row("italic", ansi.highlight(ex, italic=True))
    _print_table_row("underline", ansi.highlight(ex, underline=True))
    _print_table_row("crossout (strikethrough)", ansi.highlight(ex, crossout=True))
    _print_table_row("reverse", ansi.highlight(ex, reverse=True))
    _print_table_row("conceal", ansi.highlight(ex, conceal=True))
    _print_table_row("blink", ansi.highlight(ex, blink=True))


def _print_table_row(text: str, example: str) -> None:
    row = "{:<24}".format(text)
    row += "  "
    row += example
    print(row)


if __name__ == "__main__":
    main()
