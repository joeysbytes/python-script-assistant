from script_assistant import ansi, log
from script_assistant.ansi import Color as c
from script_assistant.log import LogLevel


THEME = {
    "screen": {"fg": c.WHITE, "bg": c.BLACK},
    "main_title": {"fg": c.BRIGHT_YELLOW, "underline": True},
    "sub_title": {"fg": c.BLACK, "bg": c.CYAN}
}


def main():
    initialize()
    main_heading()
    log.set_level(LogLevel.TRACE)

    print(ansi.highlight("ANSI Codes Disabled", **THEME["sub_title"]))
    print()
    ansi.disable()
    write_log_lines()
    print()

    ansi.enable()
    print(ansi.highlight("ANSI Codes Enabled", **THEME["sub_title"]))
    print()
    write_log_lines()
    print()
    finalize()


def initialize():
    ansi.enable()
    ansi.clear_screen(**THEME["screen"])


def main_heading():
    title = "Log Levels"
    print(ansi.highlight(title, **THEME["main_title"]))
    print()


def finalize():
    ansi.reset(full=True)
    print()


def write_log_lines():
    for name, level in LogLevel.__members__.items():
        out = "stderr" if level >= LogLevel.WARNING else "stdout"
        text = f"This is a log entry to '{out}' for LogLevel: {name}"
        log.log(level, text)


if __name__ == "__main__":
    main()
