.PHONY: build
build: install-build-tools build-dist


.PHONY: install-build-tools
install-build-tools:
	@echo "Installing / Upgrading tools required for building"
	@cd "$(CURRENT_DIR)" && python3 -m pip install --upgrade build twine wheel


.PHONY: build-dist
build-dist:
	@echo "Building packages"
	@cd "$(CURRENT_DIR)" && python3 -m build
