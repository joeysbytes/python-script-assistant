.PHONY: tests
tests: unit-tests static-tests

.PHONY: unit-tests
unit-tests:
	@echo "Running all unit tests..."
	@cd "$(TESTS_DIR)" && python -m pytest

.PHONY: static-tests
static-tests:
	@echo "Running MyPy tests..."
	@cd "$(SOURCE_DIR)" && python -m mypy .


# These tests are for individual feature testing, rather than the whole suite of tests.

.PHONY: ansi-tests
ansi-tests:
	@echo "Running ANSI unit tests..."
	@cd "$(TESTS_DIR)/ansi" && python -m pytest

.PHONY: log-tests
log-tests:
	@echo "Running log unit tests..."
	@cd "$(TESTS_DIR)/log" && python -m pytest

.PHONY: menu-tests
menu-tests:
	@echo "Running menu unit tests..."
	@cd "$(TESTS_DIR)/menu" && python -m pytest

.PHONY: validators-tests
validator-tests:
	@echo "Running validator unit tests..."
	@cd "$(TESTS_DIR)/validators" && python -m pytest
