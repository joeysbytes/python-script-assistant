.PHONY: install-local
install-local:
	@echo "Installing locally..."
	@cd "$(CURRENT_DIR)" && python3 -m pip install -e .
