# Cannot make "Press ENTER to continue" a common target because the make system
# just skips running it once it has been run once

.PHONY: demos
demos: ansi-demos log-demos menu-demos


# Menu Demos ##############################################################

.PHONY: menu-demos
menu-demos: full-menu-demo

.PHONY: full-menu-demo
full-menu-demo:
	@cd "$(DEMOS_DIR)/menu" && python3 full_menu.py
	@read -p "Press ENTER to continue..."


# Log Demos ###############################################################

.PHONY: log-demos
log-demos: log-levels-demo

.PHONY: log-levels-demo
log-levels-demo:
	@cd "$(DEMOS_DIR)/log" && python3 log_levels.py
	@read -p "Press ENTER to continue..."


# ANSI Demos ##############################################################

.PHONY: ansi-demos
ansi-demos: color-indexes-demo color-names-demo attributes-demo

.PHONY: color-indexes-demo
color-indexes-demo:
	@cd "$(DEMOS_DIR)/ansi" && python3 color_indexes.py
	@read -p "Press ENTER to continue..."

.PHONY: color-names-demo
color-names-demo:
	@cd "$(DEMOS_DIR)/ansi" && python3 color_names.py
	@read -p "Press ENTER to continue..."

.PHONY: attributes-demo
attributes-demo:
	@cd "$(DEMOS_DIR)/ansi" && python3 attributes.py
	@read -p "Press ENTER to continue..."
