.PHONY: clean
clean: clean-build

.PHONY: clean-build
clean-build: clean-egg-info-dir clean-dist-dir clean-build-dir


.PHONY: clean-dist-dir
clean-dist-dir:
	@echo "Cleaning dist directory"
	@cd "$(CURRENT_DIR)" && if [ -d "dist" ]; then rm -rf "dist"; fi

.PHONY: clean-egg-info-dir
clean-egg-info-dir:
	@echo "Cleaning .egg-info directory"
	@cd "$(SOURCE_DIR)" && if [ -d "script_assistant.egg-info" ];then rm -rf "script_assistant.egg-info"; fi

.PHONY: clean-build-dir
clean-build-dir:
	@echo "Cleaning build directory"
	@cd "$(CURRENT_DIR)" && if [ -d "build" ]; then rm -rf "build"; fi
