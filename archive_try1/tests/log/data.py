from datetime import datetime


LOG_LEVELS = list(range(0, 7))

LOG_LEVELS_INVALID = [-1, 7, "a", "", 3.14, -0.34, True, False]

LOGLEVEL_DATA = [
    {"name": "TRACE", "level": 0},
    {"name": "DEBUG", "level": 1},
    {"name": "INFO", "level": 2},
    {"name": "WARNING", "level": 3},
    {"name": "WARN", "level": 3},
    {"name": "ERROR", "level": 4},
    {"name": "CRITICAL", "level": 5},
    {"name": "CRIT", "level": 5},
    {"name": "FATAL", "level": 6}
]

CONSTANTS_DATA = [
    {"name": "MIN_LOG_LEVEL", "value": 0},
    {"name": "MAX_LOG_LEVEL", "value": 6}
]

CONFIG_DATA = [
    {"name": "enabled", "value": True},
    {"name": "log_level", "value": 3}
]

TEST_TIME = datetime(2021, 1, 2, 3, 4, 5)


# Unit Testing Theme ######################################################

# This is not a real theme, but is designed to give unique values for unit testing purposes.
CSI = '\033' + '['

TEST_THEME = {
    "format": "{timestamp},{level} --> {text}",

    "timestamp": {
        "format": "%Y-%m-%d %H:%M:%S",
        "highlight": {"fg": 0},
        "prefix": "-[",
        "prefix_highlight": {"fg": 1},
        "suffix": "]-",
        "suffix_highlight": {"fg": 2}
    },

    "level": {
        "levels": [
            {"text": "trace", "highlight": {"fg": 8}},
            {"text": "debug", "highlight": {"fg": 9}},
            {"text": "info ", "highlight": {"fg": 10}},
            {"text": "Warn ", "highlight": {"fg": 11}},
            {"text": "ERROR", "highlight": {"fg": 12}},
            {"text": "CRIT ", "highlight": {"fg": 13}},
            {"text": "FATAL",
             "highlight": {"fg": 14, "bg": 15, "intensity": 2, "blink": True}},
        ],
        "prefix": "=(",
        "prefix_highlight": {"fg": 3},
        "suffix": ")=",
        "suffix_highlight": {"fg": 4},
    },

    "text": {
        "highlight": {"fg": 5},
        "prefix": "~<",
        "prefix_highlight": {"fg": 6},
        "suffix": ">~",
        "suffix_highlight": {"fg": 7}
    }
}
