import pytest
from script_assistant.log import _check_level as check_level
from .data import LOG_LEVELS_INVALID, LOG_LEVELS


@pytest.mark.parametrize("level", LOG_LEVELS_INVALID)
def test_level_invalid_type(level):
    if (not isinstance(level, int)) or (isinstance(level, bool)):
        expected = "Log level is not an integer: " + str(level)
        with pytest.raises(TypeError) as te:
            check_level(level)
        actual = str(te.value)
        assert actual == expected


@pytest.mark.parametrize("level", LOG_LEVELS_INVALID)
def test_level_invalid_value(level):
    if (isinstance(level, int)) and (not isinstance(level, bool)):
        expected = "Log level must be between 0 and 6: " + str(level)
        with pytest.raises(ValueError) as ve:
            check_level(level)
        actual = str(ve.value)
        assert actual == expected


@pytest.mark.parametrize("level", LOG_LEVELS)
def test_level_valid(level):
    # If no exception is raised, the test passes
    check_level(level)
