from script_assistant import log


def test_enabled():
    expected_1 = True
    expected_2 = False
    log.enable()
    actual_1 = log.is_enabled()
    actual_2 = log.is_disabled()
    assert actual_1 == expected_1
    assert actual_2 == expected_2


def test_disabled():
    expected_1 = False
    expected_2 = True
    log.disable()
    actual_1 = log.is_enabled()
    actual_2 = log.is_disabled()
    assert actual_1 == expected_1
    assert actual_2 == expected_2
    log.enable()
