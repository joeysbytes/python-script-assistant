import pytest
from script_assistant.log import _log as l
from script_assistant import log, ansi
from .data import TEST_THEME, TEST_TIME, LOG_LEVELS


# Setup / Teardown ########################################################

@pytest.fixture
def setup_teardown(monkeypatch):
    # setup steps
    def mock_datetime_now():
        return TEST_TIME

    monkeypatch.setattr(log, "THEME", TEST_THEME)
    monkeypatch.setattr(log, "_get_current_timestamp", lambda: mock_datetime_now())
    log.set_level(log.LogLevel.TRACE)
    log.enable()
    ansi.disable()
    yield log

    # teardown steps
    ansi.disable()
    log.set_level(log.LogLevel.WARNING)
    log.enable()


# Unit Tests ##############################################################

@pytest.mark.parametrize("level", LOG_LEVELS)
def test_should_not_log(setup_teardown, capsys, level):
    expected = ""
    log_text = "You should not see this"
    test_level = level - 1
    if test_level >= 0:
        for i in range(0, test_level + 1):
            log.set_level(level)
            # clear out capsys
            capsys.readouterr()
            l(test_level, log_text)
            captured = capsys.readouterr()
            assert captured.out == expected
            assert captured.err == expected


@pytest.mark.parametrize("level", LOG_LEVELS)
def test_disabled(setup_teardown, capsys, level):
    expected = ""
    log.disable()
    log_text = "You should not see this"
    # clear capsys
    capsys.readouterr()
    log.log(level, log_text)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == expected


@pytest.mark.parametrize("level", LOG_LEVELS)
@pytest.mark.parametrize("enable_ansi", [True, False])
def test_log_no_ansi(setup_teardown, capsys, level, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "This is a log message"
    timestamp_text = log._build_timestamp()
    level_text = log._build_level(level)
    msg_text = log._build_text(log_text)
    expected = f"{timestamp_text},{level_text} --> {msg_text}\n"

    # clear capsys
    capsys.readouterr()
    l(level, log_text)
    captured = capsys.readouterr()
    actual_stdout = captured.out
    actual_stderr = captured.err

    if level >= 3:
        assert actual_stderr == expected
        assert actual_stdout == ""
    else:
        assert actual_stdout == expected
        assert actual_stderr == ""
