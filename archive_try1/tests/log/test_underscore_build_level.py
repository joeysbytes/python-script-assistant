import pytest
from script_assistant.log import _build_level as build_level
from script_assistant import log, ansi
from .data import TEST_THEME, LOG_LEVELS


# Setup / Teardown ########################################################

@pytest.fixture
def setup_teardown(monkeypatch):
    # setup steps
    monkeypatch.setattr(log, "THEME", TEST_THEME)
    ansi.disable()
    yield log

    # teardown steps
    ansi.disable()


# Unit Tests ##############################################################

@pytest.mark.parametrize("level", LOG_LEVELS)
def test_build_no_ansi(setup_teardown, level):
    level_text = TEST_THEME["level"]["levels"][level]["text"]
    expected = f"=({level_text})="
    actual = build_level(level)
    assert actual == expected


@pytest.mark.parametrize("level", LOG_LEVELS)
def test_build_with_ansi(setup_teardown, level):
    ansi.enable()
    level_text = TEST_THEME["level"]["levels"][level]["text"]
    start_prefix_highlight = ansi.highlight("",
                                            **TEST_THEME["level"]["prefix_highlight"],
                                            off_codes=False)
    end_prefix_highlight = ansi.highlight("",
                                          **TEST_THEME["level"]["prefix_highlight"],
                                          on_codes=False)
    start_suffix_highlight = ansi.highlight("",
                                            **TEST_THEME["level"]["suffix_highlight"],
                                            off_codes=False)
    end_suffix_highlight = ansi.highlight("",
                                          **TEST_THEME["level"]["suffix_highlight"],
                                          on_codes=False)
    start_level_highlight = ansi.highlight("",
                                           **TEST_THEME["level"]["levels"][level]["highlight"],
                                           off_codes=False)
    end_level_highlight = ansi.highlight("",
                                         **TEST_THEME["level"]["levels"][level]["highlight"],
                                         on_codes=False)
    expected = f"{start_prefix_highlight}=({end_prefix_highlight}"
    expected += f"{start_level_highlight}{level_text}{end_level_highlight}"
    expected += f"{start_suffix_highlight})={end_suffix_highlight}"
    actual = build_level(level)
    assert actual == expected
