import pytest
from .data import CONSTANTS_DATA, CONFIG_DATA
from script_assistant import log


@pytest.mark.parametrize('constant_data', CONSTANTS_DATA)
def test_constant_value(constant_data):
    expected = constant_data["value"]
    actual = getattr(log, constant_data["name"])
    assert actual == expected


@pytest.mark.parametrize('config_data', CONFIG_DATA)
def test_default_config_value(config_data):
    expected = config_data["value"]
    actual = log._CONFIG[config_data["name"]]
    assert actual == expected


def test_config_num_items():
    expected = len(CONFIG_DATA)
    actual = len(log._CONFIG)
    assert actual == expected


def test_default_log_level():
    expected = 3
    actual = log.get_level()
    assert actual == expected


def test_default_is_enabled():
    expected = True
    actual = log.is_enabled()
    assert actual == expected


def test_default_is_disabled():
    expected = False
    actual = log.is_disabled()
    assert actual == expected
