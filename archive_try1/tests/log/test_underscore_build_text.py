import pytest
from script_assistant.log import _build_text as build_text
from script_assistant import log, ansi
from .data import TEST_THEME


# Setup / Teardown ########################################################

@pytest.fixture
def setup_teardown(monkeypatch):
    # setup steps
    monkeypatch.setattr(log, "THEME", TEST_THEME)
    ansi.disable()
    yield log

    # teardown steps
    ansi.disable()


# Unit Tests ##############################################################

def test_build_no_ansi(setup_teardown):
    text = "This is a log message"
    expected = f"~<{text}>~"
    actual = build_text(text)
    assert actual == expected


def test_build_with_ansi(setup_teardown):
    ansi.enable()
    text = "This is a log message"
    start_prefix_highlight = ansi.highlight("",
                                            **TEST_THEME["text"]["prefix_highlight"],
                                            off_codes=False)
    end_prefix_highlight = ansi.highlight("",
                                          **TEST_THEME["text"]["prefix_highlight"],
                                          on_codes=False)
    start_suffix_highlight = ansi.highlight("",
                                            **TEST_THEME["text"]["suffix_highlight"],
                                            off_codes=False)
    end_suffix_highlight = ansi.highlight("",
                                          **TEST_THEME["text"]["suffix_highlight"],
                                          on_codes=False)
    start_text_highlight = ansi.highlight("",
                                          **TEST_THEME["text"]["highlight"],
                                          off_codes=False)
    end_text_highlight = ansi.highlight("",
                                        **TEST_THEME["text"]["highlight"],
                                        on_codes=False)

    expected = f"{start_prefix_highlight}~<{end_prefix_highlight}"
    expected += f"{start_text_highlight}{text}{end_text_highlight}"
    expected += f"{start_suffix_highlight}>~{end_suffix_highlight}"
    actual = build_text(text)
    assert actual == expected
