import pytest
from script_assistant.log import _build_timestamp as build_timestamp
from script_assistant import log, ansi
from .data import TEST_THEME, TEST_TIME


# Setup / Teardown ########################################################

@pytest.fixture
def setup_teardown(monkeypatch):
    # setup steps
    def mock_datetime_now():
        return TEST_TIME

    monkeypatch.setattr(log, "THEME", TEST_THEME)
    monkeypatch.setattr(log, "_get_current_timestamp", lambda: mock_datetime_now())
    ansi.disable()
    yield log

    # teardown steps
    ansi.disable()


# Unit Tests ##############################################################

def test_build_no_ansi(setup_teardown):
    expected = "-[2021-01-02 03:04:05]-"
    actual = build_timestamp()
    assert actual == expected


def test_build_with_ansi(setup_teardown):
    ansi.enable()
    start_prefix_highlight = ansi.highlight("",
                                            **TEST_THEME["timestamp"]["prefix_highlight"],
                                            off_codes=False)
    end_prefix_highlight = ansi.highlight("",
                                          **TEST_THEME["timestamp"]["prefix_highlight"],
                                          on_codes=False)
    start_suffix_highlight = ansi.highlight("",
                                            **TEST_THEME["timestamp"]["suffix_highlight"],
                                            off_codes=False)
    end_suffix_highlight = ansi.highlight("",
                                          **TEST_THEME["timestamp"]["suffix_highlight"],
                                          on_codes=False)
    start_ts_highlight = ansi.highlight("",
                                        **TEST_THEME["timestamp"]["highlight"],
                                        off_codes=False)
    end_ts_highlight = ansi.highlight("",
                                      **TEST_THEME["timestamp"]["highlight"],
                                      on_codes=False)

    expected = f"{start_prefix_highlight}-[{end_prefix_highlight}"
    expected += f"{start_ts_highlight}2021-01-02 03:04:05{end_ts_highlight}"
    expected += f"{start_suffix_highlight}]-{end_suffix_highlight}"
    actual = build_timestamp()
    assert actual == expected
