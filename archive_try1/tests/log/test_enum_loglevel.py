import pytest
from .data import LOGLEVEL_DATA
from script_assistant.log import LogLevel


def test_enum_length():
    expected = len(LOGLEVEL_DATA)
    actual = len(LogLevel.__members__.items())
    assert actual == expected


@pytest.mark.parametrize("loglevel_data", LOGLEVEL_DATA)
def test_enum_value(loglevel_data):
    expected = loglevel_data["level"]
    actual = LogLevel[loglevel_data["name"]]
    assert actual == expected
