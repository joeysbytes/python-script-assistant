import pytest
from script_assistant import log, ansi
from .data import TEST_THEME, TEST_TIME, LOG_LEVELS_INVALID, LOG_LEVELS


# Setup / Teardown ########################################################

@pytest.fixture
def setup_teardown(monkeypatch):
    # setup steps
    def mock_datetime_now():
        return TEST_TIME

    monkeypatch.setattr(log, "THEME", TEST_THEME)
    monkeypatch.setattr(log, "_get_current_timestamp", lambda: mock_datetime_now())
    log.set_level(log.LogLevel.TRACE)
    log.enable()
    ansi.disable()
    yield log

    # teardown steps
    ansi.disable()
    log.set_level(log.LogLevel.WARNING)
    log.enable()


# Unit Tests ##############################################################

@pytest.mark.parametrize("level", LOG_LEVELS_INVALID)
def test_log_invalid_level(setup_teardown, level):
    log_text = "You should not see me"
    with pytest.raises(Exception):
        log.log(level, log_text)


@pytest.mark.parametrize("level", LOG_LEVELS)
def test_log_valid_level_disabled(setup_teardown, capsys, level):
    log_text = "aaa"
    # clear capsys
    capsys.readouterr()
    log._log(level, log_text)
    expected_captured = capsys.readouterr()
    log.log(level, log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("level", LOG_LEVELS)
@pytest.mark.parametrize("enable_ansi", [True, False])
def test_log_valid_level(setup_teardown, capsys, level, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "aaa"
    # clear capsys
    capsys.readouterr()
    log._log(level, log_text)
    expected_captured = capsys.readouterr()
    log.log(level, log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_trace(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "000"
    # clear capsys
    capsys.readouterr()
    log.log(0, log_text)
    expected_captured = capsys.readouterr()
    log.trace(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_trace_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "000"
    # clear capsys
    capsys.readouterr()
    log.trace(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_debug(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "111"
    # clear capsys
    capsys.readouterr()
    log.log(1, log_text)
    expected_captured = capsys.readouterr()
    log.debug(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_debug_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "111"
    # clear capsys
    capsys.readouterr()
    log.debug(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_info(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "222"
    # clear capsys
    capsys.readouterr()
    log.log(2, log_text)
    expected_captured = capsys.readouterr()
    log.info(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_info_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "222"
    # clear capsys
    capsys.readouterr()
    log.info(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_warning(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "333"
    # clear capsys
    capsys.readouterr()
    log.log(3, log_text)
    expected_captured = capsys.readouterr()
    log.warning(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_warning_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "333"
    # clear capsys
    capsys.readouterr()
    log.warning(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_warn(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "333"
    # clear capsys
    capsys.readouterr()
    log.log(3, log_text)
    expected_captured = capsys.readouterr()
    log.warn(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_warn_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "333"
    # clear capsys
    capsys.readouterr()
    log.warn(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_error(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "444"
    # clear capsys
    capsys.readouterr()
    log.log(4, log_text)
    expected_captured = capsys.readouterr()
    log.error(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_error_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "444"
    # clear capsys
    capsys.readouterr()
    log.error(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_critical(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "555"
    # clear capsys
    capsys.readouterr()
    log.log(5, log_text)
    expected_captured = capsys.readouterr()
    log.critical(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_critical_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "555"
    # clear capsys
    capsys.readouterr()
    log.critical(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_crit(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "555"
    # clear capsys
    capsys.readouterr()
    log.log(5, log_text)
    expected_captured = capsys.readouterr()
    log.crit(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_crit_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "555"
    # clear capsys
    capsys.readouterr()
    log.crit(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_fatal(setup_teardown, capsys, enable_ansi):
    if enable_ansi:
        ansi.enable()
    log_text = "666"
    # clear capsys
    capsys.readouterr()
    log.log(6, log_text)
    expected_captured = capsys.readouterr()
    log.fatal(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected_captured.out
    assert actual_captured.err == expected_captured.err


@pytest.mark.parametrize("enable_ansi", [True, False])
def test_fatal_disabled(setup_teardown, capsys, enable_ansi):
    expected = ""
    log.disable()
    if enable_ansi:
        ansi.enable()
    log_text = "666"
    # clear capsys
    capsys.readouterr()
    log.fatal(log_text)
    actual_captured = capsys.readouterr()
    assert actual_captured.out == expected
    assert actual_captured.err == expected
