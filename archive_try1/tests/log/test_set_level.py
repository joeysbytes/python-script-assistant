import pytest
from script_assistant.log import set_level, get_level
from .data import LOG_LEVELS_INVALID, LOG_LEVELS


@pytest.mark.parametrize("level", LOG_LEVELS_INVALID)
def test_invalid_level(level):
    with pytest.raises(Exception):
        set_level(level)


@pytest.mark.parametrize("level", LOG_LEVELS)
def test_valid_level(level):
    expected = level
    set_level(level)
    actual = get_level()
    assert actual == expected
