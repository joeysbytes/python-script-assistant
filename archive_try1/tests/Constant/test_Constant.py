import pytest
from script_assistant.Constant import Constant as c

CONSTANT_DATA = [
    {"name": "EMPTY_STRING", "value": ""},
    {"name": "NEWLINE", "value": "\n"},
    {"name": "SPACE", "value": " "}
]


def test_enum_length():
    assert len(CONSTANT_DATA) == len(c.__members__.items())


@pytest.mark.parametrize("constant_data", CONSTANT_DATA)
def test_value(constant_data):
    expected = constant_data["value"]
    actual = c[constant_data["name"]].value
    assert actual == expected
