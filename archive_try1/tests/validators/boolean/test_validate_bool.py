import pytest
from script_assistant.validators.boolean import validate_bool
from . import data


# Valid Values ############################################################

@pytest.mark.parametrize("value", data.VALID_VALUES)
def test_valid_value(value):
    field_name = "pytest_field"
    expected = True
    actual = validate_bool(value, field_name=field_name)
    assert actual == expected


# Check None ##############################################################

def test_none_not_allowed():
    expected = False
    actual = validate_bool(None)
    assert actual == expected


def test_none_not_allowed_exception():
    field_name = "pytest_field"
    with pytest.raises(ValueError) as ve:
        validate_bool(None, field_name=field_name)
    actual = str(ve.value)
    assert field_name in actual


def test_none_allowed():
    expected = True
    actual = validate_bool(None, none_allowed=True)
    assert actual == expected


# Check Types #############################################################

@pytest.mark.parametrize("value", data.INVALID_TYPES)
def test_invalid_type(value):
    expected = False
    actual = validate_bool(value)
    assert actual == expected


@pytest.mark.parametrize("value", data.INVALID_TYPES)
def test_invalid_type_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not a boolean: " + str(value)
    with pytest.raises(TypeError) as te:
        validate_bool(value, field_name=field_name)
    actual = str(te.value)
    assert actual == expected
