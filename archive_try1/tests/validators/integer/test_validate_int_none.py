import pytest
from script_assistant.validators.integer import validate_int


def test_none_not_allowed():
    expected = False
    actual = validate_int(None)
    assert actual == expected


def test_none_not_allowed_exception():
    field_name = "pytest_field"
    with pytest.raises(ValueError) as ve:
        validate_int(None, field_name=field_name)
    actual = str(ve.value)
    assert field_name in actual


def test_none_allowed():
    expected = True
    actual = validate_int(None, none_allowed=True)
    assert actual == expected
