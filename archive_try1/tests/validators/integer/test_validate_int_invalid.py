import pytest
from script_assistant.validators.integer import validate_int
from . import data


@pytest.mark.parametrize("value", data.INVALID_VALUES)
def test_invalid_value(value):
    expected = False
    actual = validate_int(value)
    assert actual == expected


@pytest.mark.parametrize("value", data.INVALID_VALUES)
def test_invalid_value_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not an integer: " + str(value)
    with pytest.raises(TypeError) as te:
        validate_int(value, field_name=field_name)
    actual = str(te.value)
    assert actual == expected


@pytest.mark.parametrize("value", data.FLOAT_VALUES)
def test_invalid_float_value(value):
    expected = False
    actual = validate_int(value)
    assert actual == expected


@pytest.mark.parametrize("value", data.FLOAT_VALUES)
def test_invalid_float_value_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not an integer: " + str(value)
    with pytest.raises(TypeError) as te:
        validate_int(value, field_name=field_name)
    actual = str(te.value)
    assert actual == expected


@pytest.mark.parametrize("value", data.BOOL_VALUES)
def test_invalid_bool_value(value):
    expected = False
    actual = validate_int(value)
    assert actual == expected


@pytest.mark.parametrize("value", data.BOOL_VALUES)
def test_invalid_bool_value_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not an integer: " + str(value)
    with pytest.raises(TypeError) as te:
        validate_int(value, field_name=field_name)
    actual = str(te.value)
    assert actual == expected


# Invalid Strings #########################################################

@pytest.mark.parametrize("value", data.INVALID_STR_VALUES)
def test_invalid_str_value(value):
    expected = False
    actual = validate_int(value, str_allowed=True)
    assert actual == expected


@pytest.mark.parametrize("value", data.INVALID_STR_VALUES)
def test_invalid_str_value_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not a valid string integer: " + value
    with pytest.raises(ValueError) as ve:
        validate_int(value, str_allowed=True, field_name=field_name)
    actual = str(ve.value)
    assert actual == expected


@pytest.mark.parametrize("value", data.INVALID_STR_VALUES)
def test_invalid_str_value_float_allowed(value):
    expected = False
    actual = validate_int(value, str_allowed=True, float_allowed=True)
    assert actual == expected


@pytest.mark.parametrize("value", data.INVALID_STR_VALUES)
def test_invalid_str_value_float_allowed_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not a valid string integer: " + value
    with pytest.raises(ValueError) as ve:
        validate_int(value, str_allowed=True, float_allowed=True, field_name=field_name)
    actual = str(ve.value)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_INT_VALUES)
def test_invalid_str_int_value(value):
    expected = False
    actual = validate_int(value)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_INT_VALUES)
def test_invalid_str_int_value_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not an integer: " + str(value)
    with pytest.raises(TypeError) as te:
        validate_int(value, field_name=field_name)
    actual = str(te.value)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_FLOAT_VALUES)
def test_invalid_str_float_value(value):
    expected = False
    actual = validate_int(value)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_FLOAT_VALUES)
def test_invalid_str_float_value_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not an integer: " + str(value)
    with pytest.raises(TypeError) as te:
        validate_int(value, field_name=field_name)
    actual = str(te.value)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_FLOAT_VALUES)
def test_invalid_str_float_value_str_allowed(value):
    expected = False
    actual = validate_int(value, str_allowed=True)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_FLOAT_VALUES)
def test_invalid_str_float_value_str_allowed_exception(value):
    field_name = "pytest_field"
    expected = field_name + " is not a valid string integer: " + value
    with pytest.raises(ValueError) as ve:
        validate_int(value, str_allowed=True, field_name=field_name)
    actual = str(ve.value)
    assert actual == expected
