import pytest
from script_assistant.validators.integer import validate_int
from . import data


@pytest.mark.parametrize("value", data.INT_VALUES)
def test_valid_value_int(value):
    field_name = "pytest_field"
    expected = True
    actual = validate_int(value, field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.BOOL_VALUES)
def test_valid_value_bool(value):
    field_name = "pytest_field"
    expected = True
    actual = validate_int(value, bool_allowed=True, field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.FLOAT_VALUES)
def test_valid_value_float(value):
    field_name = "pytest_field"
    expected = True
    actual = validate_int(value, float_allowed=True, field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_INT_VALUES)
def test_valid_value_str_int(value):
    field_name = "pytest_field"
    expected = True
    actual = validate_int(value, str_allowed=True, field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_INT_VALUES)
def test_valid_value_str_int_float_allowed(value):
    field_name = "pytest_field"
    expected = True
    actual = validate_int(value, str_allowed=True, float_allowed=True, field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.STR_FLOAT_VALUES)
def test_valid_value_str_float(value):
    field_name = "pytest_field"
    expected = True
    actual = validate_int(value, str_allowed=True, float_allowed=True, field_name=field_name)
    assert actual == expected
