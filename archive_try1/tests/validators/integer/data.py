INT_VALUES = [-1, 0, 1, -5, 7]
FLOAT_VALUES = [-1.0, -5.75, 0.0, 1.0, 3.141592654]
BOOL_VALUES = [True, False]
INVALID_VALUES = [{0: 1, 4: 5}, [2, 3]]

STR_INT_VALUES = ["-1", "0", "1", "-5", "7"]
STR_FLOAT_VALUES = ["-1.0", "-5.75", "0.0", "1.0", "3.141592654"]
INVALID_STR_VALUES = ["", " ", "a", "10a"]

ONE_VALUES = [1, 1.7, "1", "1.7", True]
