import pytest
from script_assistant.validators.integer import validate_int
from . import data


# Check Minimum Value #####################################################

@pytest.mark.parametrize("value", data.ONE_VALUES)
def test_min_value_valid(value):
    field_name = "pytest_field"
    expected = True
    min_value = 0
    actual = validate_int(value,
                          min_value=min_value,
                          float_allowed=True,
                          str_allowed=True,
                          bool_allowed=True,
                          field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.ONE_VALUES)
def test_min_value_valid_equal(value):
    field_name = "pytest_field"
    expected = True
    min_value = 1
    actual = validate_int(value,
                          min_value=min_value,
                          float_allowed=True,
                          str_allowed=True,
                          bool_allowed=True,
                          field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.ONE_VALUES)
def test_min_value_invalid(value):
    expected = False
    min_value = 2
    actual = validate_int(value,
                          min_value=min_value,
                          float_allowed=True,
                          str_allowed=True,
                          bool_allowed=True)
    assert actual == expected


@pytest.mark.parametrize("value", data.ONE_VALUES)
def test_min_value_invalid_exception(value):
    field_name = "pytest_field"
    min_value = 2
    expected = field_name + " < minimum value: " + "1" + " < " + str(min_value)
    with pytest.raises(ValueError) as ve:
        validate_int(value,
                     min_value=min_value,
                     float_allowed=True,
                     str_allowed=True,
                     bool_allowed=True,
                     field_name=field_name)
    actual = str(ve.value)
    assert actual == expected


# Check Maximum Value #####################################################

@pytest.mark.parametrize("value", data.ONE_VALUES)
def test_max_value_valid(value):
    field_name = "pytest_field"
    expected = True
    max_value = 2
    actual = validate_int(value,
                          max_value=max_value,
                          float_allowed=True,
                          str_allowed=True,
                          bool_allowed=True,
                          field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.ONE_VALUES)
def test_max_value_valid_equal(value):
    field_name = "pytest_field"
    expected = True
    max_value = 1
    actual = validate_int(value,
                          max_value=max_value,
                          float_allowed=True,
                          str_allowed=True,
                          bool_allowed=True,
                          field_name=field_name)
    assert actual == expected


@pytest.mark.parametrize("value", data.ONE_VALUES)
def test_max_value_invalid(value):
    expected = False
    max_value = 0
    actual = validate_int(value,
                          max_value=max_value,
                          float_allowed=True,
                          str_allowed=True,
                          bool_allowed=True)
    assert actual == expected


@pytest.mark.parametrize("value", data.ONE_VALUES)
def test_max_value_invalid_exception(value):
    field_name = "pytest_field"
    max_value = 0
    expected = field_name + " > maximum value: " + "1" + " > " + str(max_value)
    with pytest.raises(ValueError) as ve:
        validate_int(value,
                     max_value=max_value,
                     float_allowed=True,
                     str_allowed=True,
                     bool_allowed=True,
                     field_name=field_name)
    actual = str(ve.value)
    assert actual == expected
