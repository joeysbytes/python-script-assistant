import pytest
from script_assistant.validators.none import validate_none


def test_none_not_allowed():
    expected = False
    actual = validate_none(None)
    assert actual == expected


def test_none_not_allowed_exception():
    field_name = "pytest_field"
    expected = field_name + " is None"
    with pytest.raises(ValueError) as ve:
        validate_none(None, field_name=field_name)
    actual = str(ve.value)
    assert actual == expected


def test_none_allowed():
    expected = True
    actual = validate_none(None, none_allowed=True)
    assert actual == expected
