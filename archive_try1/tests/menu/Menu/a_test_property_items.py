import pytest
from script_assistant import Menu
from . import data


@pytest.mark.parametrize("item_list", data.NON_ITEM_TYPES)
def test_invalid_items_type(item_list):
    expected = "Menu items must be List or Dict: " + str(type(item_list))
    with pytest.raises(TypeError) as te:
        Menu(item_list)
    actual = str(te.value)
    assert actual == expected


@pytest.mark.parametrize("item_list", data.EMPTY_ITEMS)
def test_empty_items(item_list):
    expected = "Menu items list is empty: " + str(item_list)
    with pytest.raises(ValueError) as ve:
        Menu(item_list)
    actual = str(ve.value)
    assert actual == expected


def test_single_item_list():
    expected = data.MINIMAL_MENU_ITEMS_DATA
    menu = Menu(data.MINIMAL_MENU_ITEMS)
    actual = menu._items_data
    assert actual == expected
    assert menu.items == expected["items"]


def test_single_item_dict():
    expected = data.MINIMAL_MENU_ITEMS_DICT_DATA
    menu = Menu(data.MINIMAL_MENU_ITEMS_DICT)
    actual = menu._items_data
    assert actual == expected
    assert menu.items == expected["items"]


def test_small_items_list():
    expected = data.SMALL_MENU_ITEMS_DATA
    menu = Menu(data.SMALL_MENU_ITEMS)
    actual = menu._items_data
    assert actual == expected
    assert menu.items == expected["items"]


def test_small_items_dict():
    expected = data.SMALL_MENU_ITEMS_DICT_DATA
    menu = Menu(data.SMALL_MENU_ITEMS_DICT)
    actual = menu._items_data
    assert actual == expected
    assert menu.items == expected["items"]


def test_big_items_list():
    expected = data.BIG_MENU_ITEMS_DATA
    menu = Menu(data.BIG_MENU_ITEMS)
    actual = menu._items_data
    assert actual == expected
    assert menu.items == expected["items"]


def test_big_items_dict():
    expected = data.BIG_MENU_ITEMS_DICT_DATA
    menu = Menu(data.BIG_MENU_ITEMS_DICT)
    actual = menu._items_data
    assert actual == expected
    assert menu.items == expected["items"]
