import pytest
from script_assistant import Menu
from .data import MINIMAL_MENU_ITEMS, NON_NONE_STR_VALUES


@pytest.fixture
def menu():
    m = Menu(MINIMAL_MENU_ITEMS)
    yield m


def test_prompt_default(menu):
    expected = "Enter Choice"
    actual = menu.prompt
    assert actual == expected


def test_prompt_none(menu):
    expected = ""
    menu.prompt = None
    actual = menu.prompt
    assert actual == expected


@pytest.mark.parametrize("prompt", NON_NONE_STR_VALUES)
def test_prompt_value(menu, prompt):
    expected = str(prompt)
    menu.prompt = prompt
    actual = menu.prompt
    assert actual == expected
