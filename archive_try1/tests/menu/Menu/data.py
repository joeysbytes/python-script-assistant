# These have to be 1 item in length
MINIMAL_MENU_ITEMS = ["Kitten"]
MINIMAL_MENU_ITEMS_DATA = {
    "chosen_key": None,
    "max_key_length": 1,
    "max_text_length": 6,
    "num_items": 1,
    "items": {"1": {"text": "Kitten", "index": 0}}
}

MINIMAL_MENU_ITEMS_DICT = {"kit": "Kitten"}
MINIMAL_MENU_ITEMS_DICT_DATA = {
    "chosen_key": None,
    "max_key_length": 3,
    "max_text_length": 6,
    "num_items": 1,
    "items": {"kit": {"text": "Kitten", "index": 0}}
}

SMALL_MENU_ITEMS = ["Kitten / Cat",
                    "Puppy / Dog",
                    "Calf / Cow",
                    "Joey / Kangaroo",
                    "Bunny / Rabbit"]
SMALL_MENU_ITEMS_DATA = {
    "chosen_key": None,
    "max_key_length": 1,
    "max_text_length": 15,
    "num_items": 5,
    "items": {"1": {"text": "Kitten / Cat", "index": 0},
              "2": {"text": "Puppy / Dog", "index": 1},
              "3": {"text": "Calf / Cow", "index": 2},
              "4": {"text": "Joey / Kangaroo", "index": 3},
              "5": {"text": "Bunny / Rabbit", "index": 4}}
    }

SMALL_MENU_ITEMS_DICT = {"strut": "Kitten / Cat",
                         "run": "Puppy / Dog",
                         "meander": "Calf / Cow",
                         "hop": "Joey / Kangaroo",
                         "jump": "Bunny / Rabbit"}
SMALL_MENU_ITEMS_DICT_DATA = {
    "chosen_key": None,
    "max_key_length": 7,
    "max_text_length": 15,
    "num_items": 5,
    "items": {"strut": {"text": "Kitten / Cat", "index": 0},
              "run": {"text": "Puppy / Dog", "index": 1},
              "meander": {"text": "Calf / Cow", "index": 2},
              "hop": {"text": "Joey / Kangaroo", "index": 3},
              "jump": {"text": "Bunny / Rabbit", "index": 4}}}

BIG_MENU_ITEMS = ["Memory",
                  "CPU",
                  "Hard Drive",
                  "Motherboard",
                  "Case",
                  "Power Supply",
                  "Keyboard",
                  "Trackball",
                  "Monitor",
                  "Graphics Card",
                  "Speakers",
                  "Blu-Ray Drive"]
BIG_MENU_ITEMS_DATA = {
    "chosen_key": None,
    "max_key_length": 2,
    "max_text_length": 13,
    "num_items": 12,
    "items": {"1": {"text": "Memory", "index": 0},
              "2": {"text": "CPU", "index": 1},
              "3": {"text": "Hard Drive", "index": 2},
              "4": {"text": "Motherboard", "index": 3},
              "5": {"text": "Case", "index": 4},
              "6": {"text": "Power Supply", "index": 5},
              "7": {"text": "Keyboard", "index": 6},
              "8": {"text": "Trackball", "index": 7},
              "9": {"text": "Monitor", "index": 8},
              "10": {"text": "Graphics Card", "index": 9},
              "11": {"text": "Speakers", "index": 10},
              "12": {"text": "Blu-Ray Drive", "index": 11}}
}

BIG_MENU_ITEMS_DICT = {"mem": "Memory",
                       "proc": "CPU",
                       "hd": "Hard Drive",
                       "mb": "Motherboard",
                       "case": "Case",
                       "ps": "Power Supply",
                       "kb": "Keyboard",
                       "mouse": "Trackball",
                       "screen": "Monitor",
                       "gpu": "Graphics Card",
                       "sound": "Speakers",
                       "disc": "Blu-Ray Drive"}
BIG_MENU_ITEMS_DICT_DATA = {
    "chosen_key": None,
    "max_key_length": 6,
    "max_text_length": 13,
    "num_items": 12,
    "items": {"mem": {"text": "Memory", "index": 0},
              "proc": {"text": "CPU", "index": 1},
              "hd": {"text": "Hard Drive", "index": 2},
              "mb": {"text": "Motherboard", "index": 3},
              "case": {"text": "Case", "index": 4},
              "ps": {"text": "Power Supply", "index": 5},
              "kb": {"text": "Keyboard", "index": 6},
              "mouse": {"text": "Trackball", "index": 7},
              "screen": {"text": "Monitor", "index": 8},
              "gpu": {"text": "Graphics Card", "index": 9},
              "sound": {"text": "Speakers", "index": 10},
              "disc": {"text": "Blu-Ray Drive", "index": 11}}
}

# Used for validations
NON_NONE_STR_VALUES = ["", "Main", 12, -5, 3.14, -2.8, True, False]
NON_ITEM_TYPES = ["abc", set(), None]
EMPTY_ITEMS = [[], {}]
