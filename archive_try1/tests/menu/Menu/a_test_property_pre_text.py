import pytest
from script_assistant import Menu
from .data import MINIMAL_MENU_ITEMS, NON_NONE_STR_VALUES


@pytest.fixture
def menu():
    m = Menu(MINIMAL_MENU_ITEMS)
    yield m


def test_pre_text_default(menu):
    expected = None
    actual = menu.pre_text
    assert actual == expected


def test_pre_text_none(menu):
    expected = None
    menu.pre_text = None
    actual = menu.pre_text
    assert actual == expected


@pytest.mark.parametrize("pre_text", NON_NONE_STR_VALUES)
def test_pre_text_value(menu, pre_text):
    expected = str(pre_text)
    menu.pre_text = pre_text
    actual = menu.pre_text
    assert actual == expected
