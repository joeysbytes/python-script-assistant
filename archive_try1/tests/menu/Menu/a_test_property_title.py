import pytest
from script_assistant import Menu
from .data import MINIMAL_MENU_ITEMS, NON_NONE_STR_VALUES


@pytest.fixture
def menu():
    m = Menu(MINIMAL_MENU_ITEMS)
    yield m


def test_title_default(menu):
    expected = None
    actual = menu.title
    assert actual == expected


def test_title_none(menu):
    expected = None
    menu.title = None
    actual = menu.title
    assert actual == expected


@pytest.mark.parametrize("title", NON_NONE_STR_VALUES)
def test_title_value(menu, title):
    expected = str(title)
    menu.title = title
    actual = menu.title
    assert actual == expected
