import pytest
from script_assistant import Menu
from .data import MINIMAL_MENU_ITEMS, NON_NONE_STR_VALUES


@pytest.fixture
def menu():
    m = Menu(MINIMAL_MENU_ITEMS)
    yield m


def test_post_text_default(menu):
    expected = None
    actual = menu.post_text
    assert actual == expected


def test_post_text_none(menu):
    expected = None
    menu.post_text = None
    actual = menu.post_text
    assert actual == expected


@pytest.mark.parametrize("post_text", NON_NONE_STR_VALUES)
def test_post_text_value(menu, post_text):
    expected = str(post_text)
    menu.post_text = post_text
    actual = menu.post_text
    assert actual == expected
