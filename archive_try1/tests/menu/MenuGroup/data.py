INVALID_MENU_ITEM_TYPES = [None, "test", "", 5, -3, True, False, set(), 3.14, -1.23]
EMPTY_MENU_ITEMS = [[], {}]

DUPLICATE_KEY_MENU_ITEMS = {
    "1": "Item 1",
    "b": "Item 2",
    1: "Item 1b",
    "c": "Item 3"
}
DUPLICATE_KEY = "1"

# Menu Items List #########################################################

MENU_ITEMS_LIST = [
    "Black", "Red", "Green", "Yellow",
    "Blue", "Magenta", "Cyan", "White",
    "Grey", "Bright Red", "Bright Green", "Bright Yellow",
    "Bright Blue", "Bright Magenta", "Bright Cyan", "Bright White"
]
MENU_ITEMS_LIST_MAX_KEY_LENGTH = 2
MENU_ITEMS_LIST_MAX_TEXT_LENGTH = 14
MENU_ITEMS_LIST_STR = (
    "(0) 1: Black\n" + "(1) 2: Red\n" + "(2) 3: Green\n" + "(3) 4: Yellow\n" +
    "(4) 5: Blue\n" + "(5) 6: Magenta\n" + "(6) 7: Cyan\n" + "(7) 8: White\n" +
    "(8) 9: Grey\n" + "(9) 10: Bright Red\n" + "(10) 11: Bright Green\n" + "(11) 12: Bright Yellow\n" +
    "(12) 13: Bright Blue\n" + "(13) 14: Bright Magenta\n" + "(14) 15: Bright Cyan\n" + "(15) 16: Bright White"
)
MENU_ITEMS_LIST_REPR = (
        "MenuGroup(items=" +
        "{'1': 'Black', '2': 'Red', '3': 'Green', '4': 'Yellow', " +
        "'5': 'Blue', '6': 'Magenta', '7': 'Cyan', '8': 'White', " +
        "'9': 'Grey', '10': 'Bright Red', '11': 'Bright Green', '12': 'Bright Yellow', " +
        "'13': 'Bright Blue', '14': 'Bright Magenta', '15': 'Bright Cyan', '16': 'Bright White'})")

# Menu Items List #########################################################

MENU_ITEMS_DICT = {
    "Cat": "Kitten",
    "Dog": "Puppy",
    "Frog": "Tadpole",
    "Kangaroo": "Joey",
    "Rabbit": "Bunny"
}
MENU_ITEMS_DICT_MAX_KEY_LENGTH = 8
MENU_ITEMS_DICT_MAX_TEXT_LENGTH = 7
MENU_ITEMS_DICT_STR = (
    "(0) Cat: Kitten\n" + "(1) Dog: Puppy\n" + "(2) Frog: Tadpole\n" +
    "(3) Kangaroo: Joey\n" + "(4) Rabbit: Bunny"
)
MENU_ITEMS_DICT_REPR = (
    "MenuGroup(items={'Cat': 'Kitten', 'Dog': 'Puppy', " +
    "'Frog': 'Tadpole', 'Kangaroo': 'Joey', 'Rabbit': 'Bunny'})")
