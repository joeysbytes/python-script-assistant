from script_assistant.menu.MenuGroup import MenuGroup
from . import data


def test_valid_items_list():
    actual = MenuGroup(data.MENU_ITEMS_LIST)
    assert actual.max_key_length == data.MENU_ITEMS_LIST_MAX_KEY_LENGTH
    assert actual.max_text_length == data.MENU_ITEMS_LIST_MAX_TEXT_LENGTH
    assert actual.num_items == len(data.MENU_ITEMS_LIST)
    assert str(actual) == data.MENU_ITEMS_LIST_STR
    assert repr(actual) == data.MENU_ITEMS_LIST_REPR
