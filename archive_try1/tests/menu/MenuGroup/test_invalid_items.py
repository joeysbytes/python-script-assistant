import pytest
from script_assistant.menu.MenuGroup import MenuGroup
from . import data


@pytest.mark.parametrize("items", data.INVALID_MENU_ITEM_TYPES)
def test_invalid_items_type(items):
    expected = "Menu items must be List or Dict: " + str(type(items))
    with pytest.raises(TypeError) as te:
        MenuGroup(items)
    actual = str(te.value)
    assert actual == expected


@pytest.mark.parametrize("items", data.EMPTY_MENU_ITEMS)
def test_empty_items(items):
    expected = "Menu items list is empty: " + str(items)
    with pytest.raises(ValueError) as ve:
        MenuGroup(items)
    actual = str(ve.value)
    assert actual == expected


def test_duplicate_key():
    expected = "Menu key already exists: " + data.DUPLICATE_KEY
    with pytest.raises(ValueError) as ve:
        MenuGroup(data.DUPLICATE_KEY_MENU_ITEMS)
    actual = str(ve.value)
    assert actual == expected
