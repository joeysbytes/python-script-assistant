import pytest
from script_assistant.menu.MenuItem import MenuItem
from . import data


@pytest.mark.parametrize("key", data.VALID_KEY_VALUES)
def test_valid_value(key):
    expected = str(key)
    expected_len = len(expected)
    menu_item = MenuItem(key=key, text="Menu Item", index=0)
    actual = menu_item.key
    actual_len = menu_item.key_length
    assert actual == expected
    assert actual_len == expected_len


@pytest.mark.parametrize("key", data.EMPTY_KEY_VALUES)
def test_empty_value(key):
    expected = "Menu Item key is empty or blank: '" + key + "'"
    with pytest.raises(ValueError) as ve:
        MenuItem(key, text="Menu Item", index=0)
    actual = str(ve.value)
    assert actual == expected
