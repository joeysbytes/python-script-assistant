from script_assistant.menu.MenuItem import MenuItem


def test_str():
    index = 15
    key = "abc"
    text = "Menu Item"
    expected = "(" + str(index) + ") " + key + ": " + text
    menu_item = MenuItem(key, text, index)
    actual = str(menu_item)
    assert actual == expected


def test_repr():
    index = 15
    key = "abc"
    text = "Menu Item"
    expected = ('MenuItem(key="' + key + '"' +
                ', text="' + text + '"' +
                ', index=' + str(index) + ')')
    menu_item = MenuItem(key, text, index)
    actual = repr(menu_item)
    assert actual == expected


def test_repr_clone():
    index = 15
    key = "abc"
    text = "Menu Item"
    expected = MenuItem(key, text, index)
    actual = eval(repr(expected))
    assert actual.key == expected.key
    assert actual.key_length == expected.key_length
    assert actual.text == expected.text
    assert actual.text_length == expected.text_length
    assert actual.index == expected.index
