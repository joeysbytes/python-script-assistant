import pytest
from script_assistant.menu.MenuItem import MenuItem
from . import data


@pytest.mark.parametrize("text", data.VALID_TEXT_VALUES)
def test_valid_value(text):
    expected = str(text)
    expected_len = len(expected)
    menu_item = MenuItem(key="key", text=text, index=0)
    actual = menu_item.text
    actual_len = menu_item.text_length
    assert actual == expected
    assert actual_len == expected_len
