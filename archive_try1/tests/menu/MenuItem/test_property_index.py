import pytest
from script_assistant.menu.MenuItem import MenuItem
from . import data


@pytest.mark.parametrize("index", data.VALID_INDEX_VALUES)
def test_valid_value(index):
    expected = index
    menu_item = MenuItem(key="key", text="Item 1", index=index)
    actual = menu_item.index
    assert actual == expected


@pytest.mark.parametrize("index", data.INVALID_INDEX_VALUES)
def test_invalid_value(index):
    expected = "Menu Item index < 0: " + str(index)
    with pytest.raises(ValueError) as ve:
        MenuItem(key="key", text="Item", index=index)
    actual = str(ve.value)
    assert actual == expected


@pytest.mark.parametrize("index", data.INVALID_INDEX_VALUE_TYPES)
def test_invalid_value_type(index):
    expected = "Menu Item index is not an int: " + str(type(index))
    with pytest.raises(TypeError) as te:
        MenuItem(key="key", text="Item", index=index)
    actual = str(te.value)
    assert actual == expected
