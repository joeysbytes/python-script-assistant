import pytest
from .data import CONSTANTS_DATA, CONFIG_DATA
from script_assistant import ansi


@pytest.mark.parametrize("constant_data", CONSTANTS_DATA)
def test_constant_value(constant_data):
    expected = constant_data["value"]
    actual = getattr(ansi, constant_data["name"])
    assert actual == expected


@pytest.mark.parametrize("config_data", CONFIG_DATA)
def test_default_config_value(config_data):
    ansi.reset(full=True)
    expected = config_data["value"]
    actual = ansi._CONFIG[config_data["name"]]
    assert actual == expected


def test_config_num_items():
    expected = len(CONFIG_DATA)
    actual = len(ansi._CONFIG)
    assert actual == expected


def test_default_enabled():
    ansi.reset(full=True)
    expected = False
    actual = ansi.is_enabled()
    assert actual == expected


def test_default_disabled():
    ansi.reset(full=True)
    expected = True
    actual = ansi.is_disabled()
    assert actual == expected
