import pytest
from script_assistant.ansi import (set_default_color,
                                   get_default_color,
                                   get_default_fg,
                                   get_default_bg)
from .data import COLOR_INDEXES, COLOR_INVALID_INDEXES


# Foreground ##############################################################

@pytest.mark.parametrize("color_index", COLOR_INDEXES)
def test_set_default_fg(color_index):
    expected = color_index
    set_default_color(fg=color_index)
    actual = get_default_fg()
    assert actual == expected


@pytest.mark.parametrize("color_index", COLOR_INVALID_INDEXES)
def test_set_default_fg_invalid(color_index):
    with pytest.raises(Exception):
        set_default_color(fg=color_index)


# Background ##############################################################

@pytest.mark.parametrize("color_index", COLOR_INDEXES)
def test_set_default_bg(color_index):
    expected = color_index
    set_default_color(bg=color_index)
    actual = get_default_bg()
    assert actual == expected


@pytest.mark.parametrize("color_index", COLOR_INVALID_INDEXES)
def test_set_default_bg_invalid(color_index):
    with pytest.raises(Exception):
        set_default_color(bg=color_index)


# Both ####################################################################

@pytest.mark.parametrize("color_index", COLOR_INDEXES)
def test_set_default_color(color_index):
    fg = color_index
    bg = -1 if color_index == 15 else color_index + 1
    expected = (fg, bg)
    set_default_color(fg=fg, bg=bg)
    actual = get_default_color()
    assert actual == expected
