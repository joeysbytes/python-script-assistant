COLOR_INDEXES = list(range(-1, 16))

COLOR_INVALID_INDEXES = [-2, 16, "a", "", 3.14, -0.34, True, False]

COLOR_DATA = [
    {"name": "DEFAULT", "index": -1, "fg": 39, "bg": 49},
    {"name": "BLACK", "index": 0, "fg": 30, "bg": 40},
    {"name": "RED", "index": 1, "fg": 31, "bg": 41},
    {"name": "GREEN", "index": 2, "fg": 32, "bg": 42},
    {"name": "YELLOW", "index": 3, "fg": 33, "bg": 43},
    {"name": "BLUE", "index": 4, "fg": 34, "bg": 44},
    {"name": "MAGENTA", "index": 5, "fg": 35, "bg": 45},
    {"name": "CYAN", "index": 6, "fg": 36, "bg": 46},
    {"name": "WHITE", "index": 7, "fg": 37, "bg": 47},
    {"name": "BRIGHT_BLACK", "index": 8, "fg": 90, "bg": 100},
    {"name": "GRAY", "index": 8, "fg": 90, "bg": 100},
    {"name": "GREY", "index": 8, "fg": 90, "bg": 100},
    {"name": "BRIGHT_RED", "index": 9, "fg": 91, "bg": 101},
    {"name": "BRIGHT_GREEN", "index": 10, "fg": 92, "bg": 102},
    {"name": "BRIGHT_YELLOW", "index": 11, "fg": 93, "bg": 103},
    {"name": "BRIGHT_BLUE", "index": 12, "fg": 94, "bg": 104},
    {"name": "BRIGHT_MAGENTA", "index": 13, "fg": 95, "bg": 105},
    {"name": "BRIGHT_CYAN", "index": 14, "fg": 96, "bg": 106},
    {"name": "BRIGHT_WHITE", "index": 15, "fg": 97, "bg": 107}
]

ATTRIBUTE_DATA = [
    {"name": "INTENSITY_DIM", "on": 2, "off": 22},
    {"name": "INTENSITY_NORMAL", "on": 22, "off": 22},
    {"name": "INTENSITY_BOLD", "on": 1, "off": 22},
    {"name": "ITALIC", "on": 3, "off": 23},
    {"name": "UNDERLINE", "on": 4, "off": 24},
    {"name": "CROSSOUT", "on": 9, "off": 29},
    {"name": "REVERSE", "on": 7, "off": 27},
    {"name": "CONCEAL", "on": 8, "off": 28},
    {"name": "BLINK", "on": 5, "off": 25},
]

INTENSITY_DATA = [
    {"name": "DIM", "value": 0},
    {"name": "NORMAL", "value": 1},
    {"name": "BOLD", "value": 2},
    {"name": "BRIGHT", "value": 2},
]

CONSTANTS_DATA = [
    {"name": "MIN_COLOR_INDEX", "value": -1},
    {"name": "MAX_COLOR_INDEX", "value": 15},
    {"name": "MIN_INTENSITY", "value": 0},
    {"name": "MAX_INTENSITY", "value": 2},
    {"name": "_CSI", "value": '\033' + '['},
    {"name": "_EMPTY_STRING", "value": ""}
]

CONFIG_DATA = [
    {"name": "enabled", "value": False},
    {"name": "default_fg", "value": -1},
    {"name": "default_bg", "value": -1}
]

INTENSITY_INVALID_VALUES = [-1, 3, "b", "", -0.3, 1.23, True, False]

NON_STRING_VALUES = [-3, 2, None, 3.14, -2.8, True, False]
