from script_assistant import ansi


def test_enable():
    expected_1 = True
    expected_2 = False
    ansi.enable()
    actual_1 = ansi.is_enabled()
    actual_2 = ansi.is_disabled()
    assert actual_1 == expected_1
    assert actual_2 == expected_2


def test_disable():
    expected_1 = False
    expected_2 = True
    ansi.disable()
    actual_1 = ansi.is_enabled()
    actual_2 = ansi.is_disabled()
    assert actual_1 == expected_1
    assert actual_2 == expected_2
