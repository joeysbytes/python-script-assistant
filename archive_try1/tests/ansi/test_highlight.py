import pytest
from script_assistant import ansi
from .data import INTENSITY_INVALID_VALUES, NON_STRING_VALUES

CSI = '\033' + '['


# Text Only ###############################################################

def test_text_only():
    expected = "Unit Test"
    text = "Unit Test"
    ansi.enable()
    actual = ansi.highlight(text)
    assert actual == expected


def test_text_only_disabled():
    expected = "Unit Test"
    text = "Unit Test"
    ansi.disable()
    actual = ansi.highlight(text)
    assert actual == expected


@pytest.mark.parametrize("value", NON_STRING_VALUES)
def test_non_string_text(value):
    expected = str(value)
    ansi.enable()
    actual = ansi.highlight(value)
    assert actual == expected


@pytest.mark.parametrize("value", NON_STRING_VALUES)
def test_non_string_text_disable(value):
    expected = str(value)
    ansi.disable()
    actual = ansi.highlight(value)
    assert actual == expected


# Intensity ###############################################################

@pytest.mark.parametrize("intensity", INTENSITY_INVALID_VALUES)
def test_intensity_invalid_types(intensity):
    if (not isinstance(intensity, int)) or (isinstance(intensity, bool)):
        expected = "Intensity is not an integer: '" + str(intensity) + "'"
        text = "Unit Test"
        ansi.enable()
        with pytest.raises(TypeError) as te:
            ansi.highlight(text, intensity=intensity)
        assert str(te.value) == expected


@pytest.mark.parametrize("intensity", INTENSITY_INVALID_VALUES)
def test_intensity_invalid_values(intensity):
    if (isinstance(intensity, int)) and (not isinstance(intensity, bool)):
        expected = "Intensity must be between 0 and 2: '" + str(intensity) + "'"
        text = "Unit Test"
        ansi.enable()
        with pytest.raises(ValueError) as ve:
            ansi.highlight(text, intensity=intensity)
        assert str(ve.value) == expected


def test_intensity_dim_by_int():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}22;2m{text}{CSI}22m"
    actual = ansi.highlight(text, intensity=0)
    assert actual == expected


def test_intensity_dim_by_enum():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}22;2m{text}{CSI}22m"
    actual = ansi.highlight(text, intensity=ansi.Intensity.DIM)
    assert actual == expected


def test_intensity_normal_by_int():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}22m{text}{CSI}22m"
    actual = ansi.highlight(text, intensity=1)
    assert actual == expected


def test_intensity_normal_by_enum():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}22m{text}{CSI}22m"
    actual = ansi.highlight(text, intensity=ansi.Intensity.NORMAL)
    assert actual == expected


def test_intensity_bold_by_int():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}22;1m{text}{CSI}22m"
    actual = ansi.highlight(text, intensity=2)
    assert actual == expected


def test_intensity_bold_by_enum():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}22;1m{text}{CSI}22m"
    actual = ansi.highlight(text, intensity=ansi.Intensity.BOLD)
    assert actual == expected


def test_intensity_bright_by_enum():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}22;1m{text}{CSI}22m"
    actual = ansi.highlight(text, intensity=ansi.Intensity.BRIGHT)
    assert actual == expected


# Flag Highlights #########################################################

def test_italic():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}3m{text}{CSI}23m"
    actual = ansi.highlight(text, italic=True)
    assert actual == expected


def test_underline():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}4m{text}{CSI}24m"
    actual = ansi.highlight(text, underline=True)
    assert actual == expected


def test_crossout():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}9m{text}{CSI}29m"
    actual = ansi.highlight(text, crossout=True)
    assert actual == expected


def test_reverse():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}7m{text}{CSI}27m"
    actual = ansi.highlight(text, reverse=True)
    assert actual == expected


def test_conceal():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}8m{text}{CSI}28m"
    actual = ansi.highlight(text, conceal=True)
    assert actual == expected


def test_blink():
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}5m{text}{CSI}25m"
    actual = ansi.highlight(text, blink=True)
    assert actual == expected


# All codes testing #######################################################

def test_code_order():
    ansi.reset(full=True)
    ansi.enable()
    ansi.set_default_color(fg=7, bg=4)
    text = "Unit Test"
    expected = f"{CSI}35;43;22;1;3;4;9;7;8;5m{text}{CSI}25;28;27;29;24;23;22;44;37m"
    actual = ansi.highlight(text, fg=5, bg=3, intensity=2, italic=True, underline=True,
                            crossout=True, reverse=True, conceal=True, blink=True)
    assert actual == expected


def test_code_order_on():
    ansi.reset(full=True)
    ansi.enable()
    ansi.set_default_color(fg=7, bg=4)
    text = "Unit Test"
    expected = f"{CSI}35;43;22;1;3;4;9;7;8;5m{text}"
    actual = ansi.highlight(text, fg=5, bg=3, intensity=2, italic=True, underline=True,
                            crossout=True, reverse=True, conceal=True, blink=True,
                            off_codes=False)
    assert actual == expected


def test_code_order_off():
    ansi.reset(full=True)
    ansi.enable()
    ansi.set_default_color(fg=7, bg=4)
    text = "Unit Test"
    expected = f"{text}{CSI}25;28;27;29;24;23;22;44;37m"
    actual = ansi.highlight(text, fg=5, bg=3, intensity=2, italic=True, underline=True,
                            crossout=True, reverse=True, conceal=True, blink=True,
                            on_codes=False)
    assert actual == expected


def test_code_order_all_codes_off():
    ansi.reset(full=True)
    ansi.enable()
    ansi.set_default_color(fg=7, bg=4)
    text = "Unit Test"
    expected = f"{text}"
    actual = ansi.highlight(text, fg=5, bg=3, intensity=2, italic=True, underline=True,
                            crossout=True, reverse=True, conceal=True, blink=True,
                            on_codes=False, off_codes=False)
    assert actual == expected


def test_code_order_disabled():
    ansi.reset(full=True)
    ansi.set_default_color(fg=7, bg=4)
    text = "Unit Test"
    expected = f"{text}"
    actual = ansi.highlight(text, fg=5, bg=3, intensity=2, italic=True, underline=True,
                            crossout=True, reverse=True, conceal=True, blink=True)
    assert actual == expected
