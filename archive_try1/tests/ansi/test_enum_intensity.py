import pytest
from .data import INTENSITY_DATA
from script_assistant.ansi import Intensity


def test_enum_length():
    expected = len(INTENSITY_DATA)
    actual = len(Intensity.__members__.items())
    assert actual == expected


@pytest.mark.parametrize("intensity", INTENSITY_DATA)
def test_enum_values(intensity):
    expected = intensity["value"]
    actual = Intensity[intensity["name"]]
    assert actual == expected
