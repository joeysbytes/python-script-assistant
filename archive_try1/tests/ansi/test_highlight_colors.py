import pytest
from script_assistant import ansi
from .data import COLOR_DATA, COLOR_INVALID_INDEXES

CSI = '\033' + '['


# Foreground ##############################################################

@pytest.mark.parametrize('color_data', COLOR_DATA)
def test_fg_int(color_data):
    ansi.set_default_color(-1, -1)
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}{color_data['fg']}m{text}{CSI}39m"
    actual = ansi.highlight(text, fg=color_data['index'])
    assert actual == expected


@pytest.mark.parametrize('color_data', COLOR_DATA)
def test_fg_enum(color_data):
    ansi.set_default_color(-1, -1)
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}{color_data['fg']}m{text}{CSI}39m"
    actual = ansi.highlight(text, fg=ansi.Color[color_data['name']])
    assert actual == expected


@pytest.mark.parametrize('color_index', COLOR_INVALID_INDEXES)
def test_fg_invalid_index(color_index):
    ansi.set_default_color(-1, -1)
    ansi.enable()
    text = "Unit Test"
    with pytest.raises(Exception):
        ansi.highlight(text, fg=color_index)


# Background ##############################################################

@pytest.mark.parametrize('color_data', COLOR_DATA)
def test_bg_int(color_data):
    ansi.set_default_color(-1, -1)
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}{color_data['bg']}m{text}{CSI}49m"
    actual = ansi.highlight(text, bg=color_data['index'])
    assert actual == expected


@pytest.mark.parametrize('color_data', COLOR_DATA)
def test_bg_enum(color_data):
    ansi.set_default_color(-1, -1)
    ansi.enable()
    text = "Unit Test"
    expected = f"{CSI}{color_data['bg']}m{text}{CSI}49m"
    actual = ansi.highlight(text, bg=ansi.Color[color_data['name']])
    assert actual == expected


@pytest.mark.parametrize('color_index', COLOR_INVALID_INDEXES)
def test_bg_invalid_index(color_index):
    ansi.set_default_color(-1, -1)
    ansi.enable()
    text = "Unit Test"
    with pytest.raises(Exception):
        ansi.highlight(text, bg=color_index)
