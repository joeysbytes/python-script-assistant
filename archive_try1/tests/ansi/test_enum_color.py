import pytest
from .data import COLOR_DATA
from script_assistant.ansi import Color


def test_enum_length():
    expected = len(COLOR_DATA)
    actual = len(Color.__members__.items())
    assert actual == expected


@pytest.mark.parametrize('color_data', COLOR_DATA)
def test_index(color_data):
    expected = color_data["index"]
    actual = Color[color_data["name"]]
    assert actual == expected


@pytest.mark.parametrize('color_data', COLOR_DATA)
def test_color_value(color_data):
    expected = color_data["index"]
    actual = Color[color_data["name"]].color_value()
    assert actual == expected


@pytest.mark.parametrize('color_data', COLOR_DATA)
def test_ansi_fg_value(color_data):
    expected = color_data["fg"]
    actual = Color[color_data["name"]].ansi_fg_value()
    assert actual == expected


@pytest.mark.parametrize('color_data', COLOR_DATA)
def test_ansi_bg_value(color_data):
    expected = color_data["bg"]
    actual = Color[color_data["name"]].ansi_bg_value()
    assert actual == expected
