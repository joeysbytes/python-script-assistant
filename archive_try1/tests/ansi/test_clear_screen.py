import pytest
from script_assistant import ansi
from .data import COLOR_INVALID_INDEXES

CSI = '\033' + '['


def test_defaults(capsys):
    fg = 7
    bg = 4
    ansi.reset(full=True)
    ansi.enable()
    ansi.set_default_color(fg, bg)
    expected = f"{CSI}37;44m{CSI}2J{CSI}1;1H"
    capsys.readouterr()  # to clear out capsys
    ansi.clear_screen()
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_fg() == fg
    assert ansi.get_default_bg() == bg


def test_using_colors(capsys):
    fg = 7
    bg = 4
    ansi.reset(full=True)
    ansi.enable()
    expected = f"{CSI}37;44m{CSI}2J{CSI}1;1H"
    capsys.readouterr()  # to clear out capsys
    ansi.clear_screen(fg, bg)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_fg() == fg
    assert ansi.get_default_bg() == bg


def test_disabled(capsys):
    fg = 7
    bg = 4
    ansi.reset(full=True)
    ansi.set_default_color(fg, bg)
    expected = ""
    capsys.readouterr()  # to clear out capsys
    ansi.clear_screen()
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_fg() == fg
    assert ansi.get_default_bg() == bg


def test_disabled_using_colors(capsys):
    fg = 7
    bg = 4
    ansi.reset(full=True)
    expected = ""
    capsys.readouterr()  # to clear out capsys
    ansi.clear_screen(fg, bg)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_fg() == fg
    assert ansi.get_default_bg() == bg


@pytest.mark.parametrize("color_index", COLOR_INVALID_INDEXES)
def test_invalid_colors_fg(color_index):
    with pytest.raises(Exception):
        ansi.clear_screen(fg=color_index)


@pytest.mark.parametrize("color_index", COLOR_INVALID_INDEXES)
def test_invalid_colors_bg(color_index):
    with pytest.raises(Exception):
        ansi.clear_screen(bg=color_index)
