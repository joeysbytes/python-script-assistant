import pytest
from script_assistant import ansi

CSI = '\033' + '['


def test_defaults(capsys):
    fg = 7
    bg = 4
    default_fg = -1
    default_bg = -1
    expected = f"{CSI}0m"
    ansi.enable()
    ansi.set_default_color(fg, bg)
    capsys.readouterr()  # to clear out capsys
    ansi.reset()
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_color() == (default_fg, default_bg)
    assert ansi.is_enabled() is True


def test_defaults_disabled(capsys):
    fg = 7
    bg = 4
    default_fg = -1
    default_bg = -1
    expected = ""
    ansi.disable()
    ansi.set_default_color(fg, bg)
    capsys.readouterr()  # to clear out capsys
    ansi.reset()
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_color() == (default_fg, default_bg)
    assert ansi.is_enabled() is False


def test_clear(capsys):
    fg = 7
    bg = 4
    default_fg = -1
    default_bg = -1
    expected = f"{CSI}0m{CSI}39;49m{CSI}2J{CSI}1;1H"
    ansi.enable()
    ansi.set_default_color(fg, bg)
    capsys.readouterr()  # to clear out capsys
    ansi.reset(clear=True)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_color() == (default_fg, default_bg)
    assert ansi.is_enabled() is True


def test_clear_disabled(capsys):
    fg = 7
    bg = 4
    default_fg = -1
    default_bg = -1
    expected = ""
    ansi.disable()
    ansi.set_default_color(fg, bg)
    capsys.readouterr()  # to clear out capsys
    ansi.reset(clear=True)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_color() == (default_fg, default_bg)
    assert ansi.is_enabled() is False


def test_full(capsys):
    fg = 7
    bg = 4
    default_fg = -1
    default_bg = -1
    expected = f"{CSI}0m"
    ansi.enable()
    ansi.set_default_color(fg, bg)
    capsys.readouterr()  # to clear out capsys
    ansi.reset(full=True)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_color() == (default_fg, default_bg)
    assert ansi.is_enabled() is False


def test_full_disabled(capsys):
    fg = 7
    bg = 4
    default_fg = -1
    default_bg = -1
    expected = ""
    ansi.disable()
    ansi.set_default_color(fg, bg)
    capsys.readouterr()  # to clear out capsys
    ansi.reset(full=True)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_color() == (default_fg, default_bg)
    assert ansi.is_enabled() is False


def test_clear_and_full(capsys):
    fg = 7
    bg = 4
    default_fg = -1
    default_bg = -1
    expected = f"{CSI}0m{CSI}39;49m{CSI}2J{CSI}1;1H"
    ansi.enable()
    ansi.set_default_color(fg, bg)
    capsys.readouterr()  # to clear out capsys
    ansi.reset(clear=True, full=True)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_color() == (default_fg, default_bg)
    assert ansi.is_enabled() is False


def test_clear_and_full_disabled(capsys):
    fg = 7
    bg = 4
    default_fg = -1
    default_bg = -1
    expected = ""
    ansi.disable()
    ansi.set_default_color(fg, bg)
    capsys.readouterr()  # to clear out capsys
    ansi.reset(clear=True, full=True)
    captured = capsys.readouterr()
    assert captured.out == expected
    assert captured.err == ""
    assert ansi.get_default_color() == (default_fg, default_bg)
    assert ansi.is_enabled() is False
