import pytest
from .data import ATTRIBUTE_DATA
from script_assistant.ansi import _Attribute as Attribute


def test_enum_length():
    expected = len(ATTRIBUTE_DATA)
    actual = len(Attribute.__members__.items())
    assert actual == expected


@pytest.mark.parametrize("attribute_data", ATTRIBUTE_DATA)
def test_index(attribute_data):
    expected = attribute_data["on"]
    actual = Attribute[attribute_data["name"]]
    assert actual == expected


@pytest.mark.parametrize("attribute_data", ATTRIBUTE_DATA)
def test_on_value(attribute_data):
    expected = attribute_data["on"]
    actual = Attribute[attribute_data["name"]].on_value()
    assert actual == expected


@pytest.mark.parametrize("attribute_data", ATTRIBUTE_DATA)
def test_off_value(attribute_data):
    expected = attribute_data["off"]
    actual = Attribute[attribute_data["name"]].off_value()
    assert actual == expected
