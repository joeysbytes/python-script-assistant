import pytest
from script_assistant.ansi import _check_color_index as check_color_index
from .data import COLOR_INDEXES, COLOR_INVALID_INDEXES


# Foreground ##############################################################

@pytest.mark.parametrize("color_index", COLOR_INDEXES)
def test_fg_index(color_index):
    # If no exception is raised, test passes
    check_color_index(color_index)


@pytest.mark.parametrize("color_index", COLOR_INVALID_INDEXES)
def test_fg_index_invalid_type(color_index):
    if (not isinstance(color_index, int)) or (isinstance(color_index, bool)):
        expected = ("Foreground color index is not an integer: '"
                    + str(color_index) + "'")
        with pytest.raises(TypeError) as te:
            check_color_index(color_index)
        assert str(te.value) == expected


@pytest.mark.parametrize("color_index", COLOR_INVALID_INDEXES)
def test_fg_index_invalid_value(color_index):
    if (isinstance(color_index, int)) and (not isinstance(color_index, bool)):
        expected = ("Foreground color index must be between -1 and 15: '"
                    + str(color_index) + "'")
        with pytest.raises(ValueError) as ve:
            check_color_index(color_index)
        assert str(ve.value) == expected


# Background ##############################################################

@pytest.mark.parametrize("color_index", COLOR_INDEXES)
def test_bg_index(color_index):
    # If no exception is raised, test passes
    check_color_index(color_index, background=True)


@pytest.mark.parametrize("color_index", COLOR_INVALID_INDEXES)
def test_bg_index_invalid_type(color_index):
    if (not isinstance(color_index, int)) or (isinstance(color_index, bool)):
        expected = ("Background color index is not an integer: '"
                    + str(color_index) + "'")
        with pytest.raises(TypeError) as te:
            check_color_index(color_index, background=True)
        assert str(te.value) == expected


@pytest.mark.parametrize("color_index", COLOR_INVALID_INDEXES)
def test_bg_index_invalid_value(color_index):
    if (isinstance(color_index, int)) and (not isinstance(color_index, bool)):
        expected = ("Background color index must be between -1 and 15: '"
                    + str(color_index) + "'")
        with pytest.raises(ValueError) as ve:
            check_color_index(color_index, background=True)
        assert str(ve.value) == expected
