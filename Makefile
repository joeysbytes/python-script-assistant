SHELL := bash
CURRENT_DIR := $(shell pwd)
MAKE_DIR := $(CURRENT_DIR)/make
# SOURCE_DIR := $(CURRENT_DIR)/src
# TESTS_DIR := $(CURRENT_DIR)/tests
# DEMOS_DIR := $(CURRENT_DIR)/demos

.PHONY: help
help:
	@echo ""
	@cat "$(MAKE_DIR)/help.txt"
	@echo ""
